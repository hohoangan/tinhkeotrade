﻿using LiveCharts;
using LiveCharts.Wpf;
using MaterialDesignThemes.Wpf;
using QuanLy.Models;
using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : UserControl
    {
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }
        ViewModel viewModel;
        public Dashboard()
        {
            InitializeComponent();
            LoadSheduleAndBirthDay();
            LoadChart();
        }
        void LoadChart()
        {
            try
            {
                var nhanvien = viewModel.StaffContracts.OrderByDescending(x => x.FromDate).GroupBy(x => x.StaffId).Select(x=>x.First());
                var list = (from nv in nhanvien
                            join staff in viewModel.Staffs.Where(x=>x.Status != "Đã nghỉ") on nv.StaffId equals staff.Id
                           join pos in viewModel.Positions on nv.PositionId equals pos.Id
                           select new { 
                               nv,
                               staff,
                               pos
                           }).ToList();

                var listCount = (from male in list
                                group male by new { male.pos.Department } into g
                                select new
                                {
                                    g.Key.Department.Name,
                                    totalM = g.Count(c => c.staff.Sex == true),
                                    totalF = g.Count(c => c.staff.Sex == false)
                                }).OrderBy(x=>x.Name).ToList();

                SeriesCollection = new SeriesCollection
                {
                    new StackedColumnSeries
                    {
                        Values = new ChartValues<int>(listCount.Select(x=>x.totalM)),// new ChartValues<double> {4, 5, 6, 1,2},//{ "Chrome", "Mozilla", "Opera" }
                        //StackMode = StackMode.Values, // this is not necessary, values is the default stack mode
                        DataLabels = true,
                        FontSize = 15,
                        Title = "Nam"
                    }
                    ,
                    new StackedColumnSeries
                    {
                        Values = new ChartValues<int>(listCount.Select(x=>x.totalF)),
                        //StackMode = StackMode.Values,
                        DataLabels = true,
                        FontSize = 15,
                        Title ="Nữ"
                    }
                };
                Labels = listCount.Select(x => x.Name).ToArray();
                //Formatter = value => value + "";//them chữ phía sau value

                DataContext = this;
                List<Staff> st = viewModel.Staffs.Where(x => x.Status != "Đã nghỉ").ToList();
                string tongso = string.Format("Nam: {0}\nNữ: {1}",st.Where(x=>x.Sex == true).Count(), st.Where(x => x.Sex == false).Count());
                lbTongso.Content = tongso;
                lbTotal.Content = st.Count();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        Button CreateButton(string name, int Id, SolidColorBrush color, bool brush = false, bool rightClick = false)
        {
            try
            {
                Button txt = new Button();
                txt.HorizontalAlignment = HorizontalAlignment.Left;
                txt.Margin = new Thickness(10);
                txt.Content = name;
                int len = name.Length * 12;
                //txt.Width = len < 80 ? 80 : len;
                txt.Tag = Id;
                txt.Foreground = Brushes.Black;
                if (brush)
                {
                    txt.Background = color;
                    txt.BorderBrush = color;
                }
                //tao menucontext
                if (rightClick)
                {
                    txt.ToolTip = "Click chuột phải để xóa";
                    ContextMenu contextMenu = new ContextMenu();
                    //thêm nút xóa
                    MenuItem item1 = new MenuItem();
                    item1.Header = "Xóa";
                    item1.Tag = Id;//stepId
                    item1.Click += ItemDEL_Click; 
                    contextMenu.Items.Add(item1);

                    txt.ContextMenu = contextMenu;
                }
                return txt;
            }
            catch (Exception)
            {
                return new Button();
            }
        }

        private void ItemDEL_Click(object sender, RoutedEventArgs e)
        {
        }

        void LoadBirthDay()
        {
            try
            {
                var listBirthDay = viewModel.Staffs.Where(x => x.BirthDay.Date.Month == DateTime.Now.Date.Month 
                                                            && x.BirthDay.Date.Year!=1
                                                            && x.Status!= "Đã nghỉ").OrderBy(x=>x.BirthDay.Date.ToString("MMdd")).ToList();
                WrapPanel stc = new WrapPanel();
                stc.Orientation = Orientation.Horizontal;
                listBirthDay?.ForEach(item =>
                {
                    //ADD BIRTHDAY
                    Button txt = CreateButton(string.Format("{0} : {1}", item.FullName, item.BirthDay.Date.ToString("dd/MM")) , item.Id, Brushes.Lime, true, false);
                    stc.Children.Add(txt);
                });
                stackPanelStep.Children.Add(stc);
                scrollEvent.ScrollToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadHoliday()
        {
            try
            {
                var listHoliday = viewModel.Holidays.Where(x => x.Date.Month == DateTime.Now.Month
                                                            && x.Date.Year == DateTime.Now.Year
                                                            && x.Date.Day >= DateTime.Now.Day).OrderBy(x => x.Date).ToList();
                WrapPanel stc = new WrapPanel();
                stc.Orientation = Orientation.Horizontal;
                listHoliday?.ForEach(item =>
                {
                    //ADD listHoliday
                    Button txt = CreateButton(string.Format("{0} : {1}", item.Des, item.Date.ToString("dd/MM")), item.Id, Brushes.Pink, true, false);
                    stc.Children.Add(txt);
                });
                stackPanelStep.Children.Add(stc);
                scrollEvent.ScrollToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadSchedules()
        {
            try
            {
                var listSchedule = viewModel.Schedules.Where(x => x.Date.AddTicks(x.Time.TimeOfDay.Ticks) >= DateTime.Now).OrderBy(x => x.Date.AddTicks(x.Time.Ticks)).ToList();
                listSchedule?.ForEach(item =>
                {
                    WrapPanel stc = new WrapPanel();
                    //ADD SChedule
                    stc = AddSchedule(item);
                    stackPanelStep.Children.Add(stc);
                });
                scrollEvent.ScrollToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void LoadSheduleAndBirthDay()
        {
            viewModel = new ViewModel();
            scrollEvent.Height = Constant.windowHeight;
            stackPanelStep.Children.Clear();
            LoadBirthDay();
            LoadHoliday();
            LoadSchedules();
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //tao lich hen mặc định
            WrapPanel stc = new WrapPanel();
            using (AppDbContext context = new AppDbContext())
            {
                var schedule = new Schedule
                {
                    Date = DateTime.Now.Date,
                    Time = (new DateTime()).AddTicks(DateTime.Now.TimeOfDay.Ticks),                
                };
                context.Schedules.Add(schedule);
                context.SaveChanges();

                stc = AddSchedule(schedule);

                stackPanelStep.Children.Add(stc);
                scrollEvent.ScrollToEnd();
            }
        }
        WrapPanel AddSchedule(Schedule schedule)
        {
            try
            {
                WrapPanel stc = new WrapPanel();
                stc.Orientation = Orientation.Horizontal;
                //Add Del
                Button btnSchedule = CreateButton("Xóa",schedule.Id, new SolidColorBrush(), false,false);
                btnSchedule.Click += BtnSchedule_Click;
                btnSchedule.Style = (Style)Application.Current.Resources["MaterialDesignFloatingActionMiniAccentButton"];
                stc.Children.Add(btnSchedule);
                //add date
                DatePicker date = new DatePicker();
                date.Margin = new Thickness(10);
                date.Style = (Style)Application.Current.Resources["MaterialDesignDatePicker"];
                date.SelectedDate = schedule.Date.Date;
                date.SelectedDateChanged += Date_SelectedDateChanged;
                date.Tag = schedule.Id;
                stc.Children.Add(date);
                //Add time
                TimePicker time = new TimePicker();
                time.Margin = new Thickness(10);
                time.Style = (Style)Application.Current.Resources["MaterialDesignTimePicker"];
                time.SelectedTime = schedule.Time;
                time.Tag = schedule.Id;
                time.SelectedTimeChanged += Time_SelectedTimeChanged;
                stc.Children.Add(time);
                //add des
                TextBox tb = new TextBox();
                tb.Height = 25;
                tb.Width = 200;
                tb.Tag = schedule.Id;
                tb.Text = schedule.Des;
                tb.TextChanged += Tb_TextChanged;
                stc.Children.Add(tb);
                return stc;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #region Update data
        private void Date_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                DatePicker t = sender as DatePicker;
                using (AppDbContext context = new AppDbContext())
                {
                    var sch = context.Schedules.Find((int)t.Tag);
                    sch.Date = t.SelectedDate ?? DateTime.Now;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Time_SelectedTimeChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {
            try
            {
                TimePicker t = sender as TimePicker;
                using (AppDbContext context = new AppDbContext())
                {
                    var sch = context.Schedules.Find((int)t.Tag);
                    sch.Time = t.SelectedTime ?? DateTime.Now;
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox txt = sender as TextBox;
                using (AppDbContext context = new AppDbContext())
                {
                    var sch = context.Schedules.Find((int)txt.Tag);
                    sch.Des = txt.Text.Trim();
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BtnSchedule_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                using (AppDbContext context = new AppDbContext())
                {
                    var sch = context.Schedules.Where(x=>x.Id == (int)btn.Tag).FirstOrDefault();
                    context.Schedules.Remove(sch);
                    context.SaveChanges();
                }
                LoadSheduleAndBirthDay();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #endregion
    }
}
