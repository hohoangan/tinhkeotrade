﻿using DocumentFormat.OpenXml.Drawing;
using Microsoft.Win32;
using QuanLy.Extensions;
using QuanLy.Models;
using QuanLy.ModelViews;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for TimeKeepingUC.xaml
    /// </summary>
    public partial class TimeKeepingUC : UserControl
    {
        List<FileChamCong> lstFileChamCong;
        ViewModel viewModel;
        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        public TimeKeepingUC()
        {
            InitializeComponent();
            try
            {

                lstFileChamCong = new List<FileChamCong>();
                cboMonth.Text = DateTime.Now.Month.ToString();
                ChamCongConstant.axCZKEM1 = new zkemkeeper.CZKEM();
                // Set up the SUB Background Worker Events 
                _backgroundWorker.DoWork += _backgroundWorker_DoWork;
                _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #region READ
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            viewModel = new ViewModel();
            lstFileChamCong = new List<FileChamCong>();
            gridView.ItemsSource = lstFileChamCong;
            string filename = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel Workbook |*.xlsx|Excel 97-2003 Workbook|*.xls";
            try
            {
                if (openFileDialog.ShowDialog() == true)
                {
                    filename = openFileDialog.FileName;
                    ReadExcel2DataGrid(filename);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
            finally { }
        }
        void ReadExcel2DataGrid(string filename)
        {
            try
            {
                DataTable dt = Excel.ReadExcel2DatatTable(filename, 2, 8);//row: 3: col:9
                foreach (DataRow item in dt.Rows)
                {
                    if (item[4].Equals(string.Empty))
                        continue;
                    DateTime days = DateTime.FromOADate(double.Parse(item[4].ToString()));
                    lstFileChamCong.Add(new FileChamCong
                    {
                        Code = item[1].ToString(),
                        FullName = item[2].ToString(),
                        Date = days,
                        Start = Excel.ConvertTimeSpanFromString(item[6].ToString()),
                        End = Excel.ConvertTimeSpanFromString(item[7].ToString()),
                        DayOfWeek = days.DayOfWeek
                    });
                }

                using (AppDbContext context = new AppDbContext())
                {
                    var data = from cong in lstFileChamCong
                               join nv in context.Staffs.Where(x => x.Status != "Đã nghỉ") on cong.Code equals nv.Code
                               join ca in context.Shifts on nv.ShiftId equals ca.Id
                               join mi in context.Missions on new { nv.Id, cong.Date } equals new { Id = mi.StaffId, Date = mi.FromDate } into missG
                               from miss in missG.DefaultIfEmpty()
                               select new FileChamCong
                               {
                                   Code = nv.Code,
                                   FullName = nv.FullName,
                                   Date = cong.Date,
                                   Start = cong.Start,
                                   End = cong.End,
                                   DayOfWeek = cong.DayOfWeek,
                                   //tinh tre/som
                                   /*
                                    * neu co phep ngay do bsang/chieu/ca ngay thi cộng vô giờ trễ sớm luôn
                                    */
                                   Late = CheckLate(cong, ca, miss, nv),
                                   Early = CheckEarly(cong, ca, miss, nv),
                                   Des = CheckTimeDown(miss, nv, cong.Date)
                               };

                    lstFileChamCong = data.ToList();
                    gridView.ItemsSource = data.ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
        }
        string CheckTimeDown(Mission miss, Staff nv, DateTime Date)
        {
            try
            {
                var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id)
                                              .Where(x => x.FromDate <= Date && Date <= x.ToDate).FirstOrDefault();
                string ss = "";
                if (down != null)
                    ss += string.Format("{0}: {1}", down.Reason, down.Time);

                ss += miss != null ? string.Format(" | {0} {1}: {2}", miss.Reason, miss.Time, miss.TotalHours) : "";
                return ss;
            }
            catch (Exception)
            {
                return "";
            }
        }
        TimeSpan CheckLate(FileChamCong cong, Shift ca, Mission miss, Staff nv)
        {
            //neu la ngay le thi bo qua
            var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(cong.Date.Date)).FirstOrDefault();
            if (holiday != null)
                return new TimeSpan();
            if (cong.DayOfWeek.Equals(DayOfWeek.Sunday))
                return new TimeSpan();

            //check downtime
            var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id && !x.Time.Equals("Chiều"))
                                          .Where(x => x.FromDate <= cong.Date && cong.Date <= x.ToDate).FirstOrDefault();
            if (down != null)
                return new TimeSpan();

            //cong tac
            TimeSpan congtac = (!miss?.Time.Equals("Chiều") ?? false)
                                ? (miss.TotalHours > new TimeSpan(4, 0, 0) ? new TimeSpan(4, 0, 0) : miss.TotalHours)
                                : new TimeSpan();

            //neu thoi gian cong tac >=4h thi cho phep nghi luon
            if (congtac >= new TimeSpan(4, 0, 0))
                return new TimeSpan();

            //miss punch
            if (cong.Start.Equals(new TimeSpan()))
                return new TimeSpan(4, 0, 0);

            TimeSpan tresom = ca.Start < cong.Start ? cong.Start - ca.Start : new TimeSpan();
            TimeSpan kq = tresom - congtac;

            if (kq > new TimeSpan(4, 0, 0))
                return new TimeSpan(4, 0, 0);
            else if (kq < new TimeSpan())
                return new TimeSpan();
            return kq;
        }
        TimeSpan CheckEarly(FileChamCong cong, Shift ca, Mission miss, Staff nv)
        {
            //neu la ngay le thi bo qua
            var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(cong.Date.Date)).FirstOrDefault();
            if (holiday != null)
                return new TimeSpan();

            TimeSpan End = ca.End;
            if (cong.DayOfWeek.Equals(DayOfWeek.Sunday))
                return new TimeSpan();

            //check downtime
            var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id && !x.Time.Equals("Sáng"))
                                          .Where(x => x.FromDate <= cong.Date && cong.Date <= x.ToDate).FirstOrDefault();
            if (down != null)
                return new TimeSpan();

            if (cong.DayOfWeek.Equals(DayOfWeek.Saturday))
            {
                //neu ca tu 8h thi tinh gio ra la 12AM
                //neu ca tu 9h thi tinh gio ra la 13PM
                End = ca.Sat;
                if (cong.Start.Equals(new TimeSpan()))
                    return new TimeSpan();
            }
            //cong tac
            TimeSpan congtac = (!miss?.Time.Equals("Sáng") ?? false)
                                ? (miss.TotalHours > new TimeSpan(4, 0, 0) ? new TimeSpan(4, 0, 0) : miss.TotalHours)
                                : new TimeSpan();
            //neu ngay cong tac >=4h thi cho phep nghi luon
            if (congtac >= new TimeSpan(4, 0, 0))
                return new TimeSpan();

            //miss punch
            if (cong.End.Equals(new TimeSpan()))
                return new TimeSpan(4, 0, 0);

            TimeSpan tresom = End > cong.End ? End - cong.End : new TimeSpan();
            TimeSpan kq = tresom - congtac;

            if (kq > new TimeSpan(4, 0, 0))
                return new TimeSpan(4, 0, 0);
            else if (kq < new TimeSpan())
                return new TimeSpan();
            return kq;
        }
        #endregion
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            XuatExcel();
        }
        void XuatExcel()
        {
            try
            {
                if (gridView.ItemsSource == null)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook|*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;

                    string fileXLSTemplate = Helps.GetBasePath("ChamCong.xls");
                    //Create an instance of ExcelEngine
                    using (ExcelEngine excelEngine = new ExcelEngine())
                    {
                        //Instantiate the Excel application object
                        IApplication application = excelEngine.Excel;

                        //Set the default application version
                        application.DefaultVersion = ExcelVersion.Excel2016;

                        //Load the existing Excel workbook into IWorkbook
                        IWorkbook workbook = application.Workbooks.Open(fileXLSTemplate);

                        //Get the first worksheet in the workbook into IWorksheet
                        IWorksheet worksheet = workbook.Worksheets[0];
                        IWorksheet worksheetTotal = workbook.Worksheets[1];

                        //In list cham cong
                        for (int i = 0; i < lstFileChamCong.Count(); i++)
                        {
                            FileChamCong data = lstFileChamCong[i];
                            string row = (i + 2).ToString();
                            //Assign some text in a cell
                            worksheet.Range["A" + row].Value = (i + 1).ToString();
                            worksheet.Range["B" + row].Text = data.Code.ToString();
                            worksheet.Range["C" + row].Text = data.FullName.ToString();
                            worksheet.Range["D" + row].DateTime = data.Date;
                            worksheet.Range["E" + row].Value = data.DayOfWeek.ToString();
                            worksheet.Range["F" + row].Value = data.Start.ToString();
                            worksheet.Range["G" + row].Value = data.End.ToString();
                            worksheet.Range["H" + row].Value = data.Late.ToString();
                            worksheet.Range["I" + row].Value = data.Early.ToString();
                            worksheet.Range["J" + row].Value = data.Des.ToString();

                            worksheet.Range["F" + row].NumberFormat = "hh:mm";
                            worksheet.Range["G" + row].NumberFormat = "hh:mm";
                            worksheet.Range["H" + row].NumberFormat = "hh:mm";
                            worksheet.Range["I" + row].NumberFormat = "hh:mm";

                            if (data.Early.Hours > 0 || data.Early.Minutes > 30)
                            {
                                worksheet.Range["I" + row].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                worksheet.Range["I" + row].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Yellow");
                            }
                            if (data.Late.Hours > 0 || data.Late.Minutes > 30)
                            {
                                worksheet.Range["H" + row].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                worksheet.Range["H" + row].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Yellow");
                            }
                        }

                        var lstTotal = lstFileChamCong.GroupBy(x => new { x.Code, x.FullName })
                                                      .OrderBy(x => x.Key.Code)
                                                      .Select(x =>
                                                      new
                                                      {
                                                          Code = x.Key.Code,
                                                          FullName = x.Key.FullName,
                                                          Late = x.Sum(c => c.Late.TotalMinutes),
                                                          Early = x.Sum(c => c.Early.TotalMinutes)
                                                      });

                        var lstOffsetDay = from nv in viewModel.Staffs
                                           join ca in viewModel.Shifts on nv.ShiftId equals ca.Id
                                           join off in viewModel.OffsetDays on nv.Id equals off.StaffId
                                           join cong in lstFileChamCong on new { nv.Code, DayOfWeek = off.Day } equals new { cong.Code, cong.DayOfWeek }
                                           group new { nv, cong, ca } by nv into g
                                           select new
                                           {
                                               Code = g.Key.Code,
                                               Minutes = Math.Round(g.Sum(x=> x.cong.End > x.ca.End ?  (x.cong.End - x.ca.End).TotalMinutes : 0))
                                           };

                        DateTime dt = lstFileChamCong.Max(x => x.Date);
                        var tongNghiPhep = viewModel.DownTimes.Where(x=>x.FromDate.ToString("yyyyMM") == dt.ToString("yyyyMM"))
                                                    .Where(x => x.MissPunch == false).GroupBy(x => new { x.StaffId })
                                                    .Select(x => new
                                                    {
                                                        StaffId = x.Key.StaffId,
                                                        TotalDays = x.Sum(c => c.TotalDay)
                                                    });

                        var tongNghi0Phep = viewModel.DownTimes.Where(x => x.FromDate.ToString("yyyyMM") == dt.ToString("yyyyMM"))
                                                    .Where(x => x.MissPunch == true).GroupBy(x => new { x.StaffId })
                                                    .Select(x => new
                                                    {
                                                        StaffId = x.Key.StaffId,
                                                        TotalDays = x.Sum(c => c.TotalDay)
                                                    });
                        var listFinal = from nv in viewModel.Staffs
                                        join cong in lstTotal on nv.Code equals cong.Code
                                        join off in lstOffsetDay on nv.Code equals off.Code into offG
                                        from off in offG.DefaultIfEmpty()
                                        join cophep in tongNghiPhep on nv.Id equals cophep.StaffId into cophepG
                                        from cophep in cophepG.DefaultIfEmpty()
                                        join kophep in tongNghi0Phep on nv.Id equals kophep.StaffId into kophepG
                                        from kophep in kophepG.DefaultIfEmpty()
                                        select new
                                        {
                                            Code = cong.Code,
                                            FullName = cong.FullName,
                                            Late = cong.Late,
                                            Early = cong.Early,
                                            Offset = off !=null ? off.Minutes : 0,
                                            Phep = cophep != null ? cophep.TotalDays.ToString() : "",
                                            KoPhep = kophep != null ? kophep.TotalDays.ToString() : ""
                                        };

                        int dem = 0;
                        foreach (var item in listFinal)
                        {
                            string row = (dem + 3).ToString();
                            worksheetTotal.Range["A" + row].Value = (dem + 1).ToString();
                            worksheetTotal.Range["B" + row].Text = item.Code.ToString();
                            worksheetTotal.Range["C" + row].Text = item.FullName.ToString();
                            double min = (item.Late + item.Early) ;
                            if (min > item.Offset)
                                min = min - item.Offset;
                            else
                                min = 0;
                            double hours = (double)(min / (60));
                            worksheetTotal.Range["D" + row].Value = Math.Round(hours, 1).ToString();
                            worksheetTotal.Range["E" + row].Value = item.Phep.ToString();
                            worksheetTotal.Range["F" + row].Value = item.KoPhep.ToString();
                            worksheetTotal.Range["G" + row].Value = item.Offset.ToString();

                            if (min > 390)
                            {
                                worksheetTotal.Range["D" + row].CellStyle.Color = System.Drawing.Color.FromName("Red");
                                worksheetTotal.Range["D" + row].CellStyle.Font.RGBColor = System.Drawing.Color.FromName("Yellow");
                            }
                            dem++;
                        }
                        //Save the Excel document
                        workbook.SaveAs(filename);
                    }

                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            //nghỉ phép
            try
            {
                MenuItem menuItem = sender as MenuItem;
                string header = menuItem.Header.ToString();
                bool miss = header.ToLower().Contains("không") ? true : false;
                string time = header.Split(":")[1].Trim();
                double totalDay = header.ToLower().Contains("cả") ? 1 : 0.5;
                var row = gridView.SelectedItem as FileChamCong;
                if (row == null)
                    return;
                var staff = viewModel.Staffs.Where(x => x.Code.Equals(row.Code)).FirstOrDefault();
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var phep = new DownTime
                    {
                        Des = "Thêm tự động từ tab chấm công",
                        FromDate = row.Date,
                        ToDate = row.Date,
                        MissPunch = miss,
                        Reason = miss ? "Không Phép" : "Có phép",
                        StaffId = staff.Id,
                        Time = time,
                        Location = "Hồ Chí Minh",
                        TotalDay = totalDay
                    };
                    _dbContext.DownTimes.Add(phep);
                    _dbContext.SaveChanges();
                    MessageBox.Show("Thêm thành công");

                    //update grid
                    switch (time)
                    {
                        case "Sáng": (gridView.SelectedItem as FileChamCong).Late = new TimeSpan(); break;
                        case "Chiều": (gridView.SelectedItem as FileChamCong).Early = new TimeSpan(); break;
                        default:
                            (gridView.SelectedItem as FileChamCong).Late = new TimeSpan();
                            (gridView.SelectedItem as FileChamCong).Early = new TimeSpan();
                            break;
                    }
                    gridView.View.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //quên chấm công
            try
            {
                MenuItem menuItem = sender as MenuItem;
                string header = menuItem.Header.ToString();
                string time = header.Split(":")[1].Trim();
                TimeSpan totalHours = header.ToLower().Contains("cả") ? new TimeSpan(8, 0, 0) : new TimeSpan(4, 0, 0);
                var row = gridView.SelectedItem as FileChamCong;
                if (row == null)
                    return;
                var staff = viewModel.Staffs.Where(x => x.Code.Equals(row.Code)).FirstOrDefault();
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var phep = new Mission
                    {
                        Des = "Thêm tự động từ tab chấm công",
                        FromDate = row.Date,
                        Reason = "Quên chấm công",
                        StaffId = staff.Id,
                        Time = time,
                        Location = "Hồ Chí Minh",
                        TotalHours = totalHours
                    };
                    _dbContext.Missions.Add(phep);
                    _dbContext.SaveChanges();
                    MessageBox.Show("Thêm thành công");

                    //update grid
                    switch (time)
                    {
                        case "Sáng": (gridView.SelectedItem as FileChamCong).Late = new TimeSpan(); break;
                        case "Chiều": (gridView.SelectedItem as FileChamCong).Early = new TimeSpan(); break;
                        default:
                            (gridView.SelectedItem as FileChamCong).Late = new TimeSpan();
                            (gridView.SelectedItem as FileChamCong).Early = new TimeSpan();
                            break;
                    }
                    gridView.View.Refresh();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            //công tác
            try
            {
                MenuItem menuItem = sender as MenuItem;
                string header = menuItem.Header.ToString();
                string time = header.Split(":")[1].Trim();
                TimeSpan totalHours = header.ToLower().Contains("cả") ? new TimeSpan(8, 0, 0) : new TimeSpan(4, 0, 0);
                var row = gridView.SelectedItem as FileChamCong;
                if (row == null)
                    return;
                var staff = viewModel.Staffs.Where(x => x.Code.Equals(row.Code)).FirstOrDefault();

                var phep = new Mission
                {
                    Des = "Thêm tự động từ tab chấm công",
                    FromDate = row.Date,
                    Reason = "Đi công tác",
                    StaffId = staff.Id,
                    Time = time,
                    Location = "Hồ Chí Minh",
                };

                PopupAddMission pop = new PopupAddMission(phep);
                pop.passControl = new PopupAddMission.PassControl(PassData);
                pop.ShowDialog();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void PassData(object sender)
        {
            try
            {
                var row = (gridView.SelectedItem as FileChamCong);
                Mission mission = ((Mission)sender);
                // Set de text of the textbox to the value of the textbox of form 2
                TimeSpan totalHours = mission.TotalHours;
                //update grid
                if (totalHours != null)
                {
                    switch (mission.Time)
                    {
                        case "Sáng": row.Late = (row.Late > totalHours) ? row.Late - totalHours : new TimeSpan(); break;
                        case "Chiều": row.Early = (row.Early > totalHours) ? row.Early - totalHours : new TimeSpan(); break;
                        default:
                            (gridView.SelectedItem as FileChamCong).Late = new TimeSpan();
                            (gridView.SelectedItem as FileChamCong).Early = new TimeSpan();
                            break;
                    }
                }
                gridView.View.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                viewModel = new ViewModel();
                lstFileChamCong = new List<FileChamCong>();
                gridView.ItemsSource = lstFileChamCong;

                if (_backgroundWorker.IsBusy)
                    return;
                SetLoadingButtuon(btnLoad, true);
                // Run the Background Worker
                _backgroundWorker.RunWorkerAsync(5000);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        #region MÁY CHAM CONG
        void LoadDataFromMCC()
        {
            try
            {
                //ChamCongConstant.bIsConnected = false;
                if (ChamCongConstant.bIsConnected == false)
                {
                    //ChamCongConstant.axCZKEM1 = new zkemkeeper.CZKEM();
                    ChamCongConstant.bIsConnected = ChamCongConstant.axCZKEM1.Connect_Net(ChamCongConstant.IPmcc, ChamCongConstant.Port);
                }
                if (!ChamCongConstant.bIsConnected)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        MessageBox.Show("Không thể kết nối MCC, kiểm tra lại mạng internet");
                    }
                    ));
                    return;
                }

                ZkemkeeperAPI zkem = new ZkemkeeperAPI();
                lstFileChamCong = zkem.lstFileChamCong;
                if (lstFileChamCong == null)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    new Action(() =>
                    {
                        MessageBox.Show("Không lấy được data");
                    }
                    ));
                    return;
                }

                this.Dispatcher.Invoke(() =>
                {
                    using (AppDbContext context = new AppDbContext())
                    {
                        var data = from cong in lstFileChamCong
                                   join nv in context.Staffs.Where(x => x.Status != "Đã nghỉ") on cong.Code equals nv.Code
                                   join ca in context.Shifts on nv.ShiftId equals ca.Id
                                   join mi in context.Missions on new { nv.Id, cong.Date } equals new { Id = mi.StaffId, Date = mi.FromDate } into missG
                                   from miss in missG.DefaultIfEmpty()
                                   select new FileChamCong
                                   {
                                       Code = nv.Code,
                                       FullName = nv.FullName,
                                       Date = cong.Date,
                                       Start = cong.Start,
                                       End = cong.End,
                                       DayOfWeek = cong.DayOfWeek,
                                       //tinh tre/som
                                       /*
                                        * neu co phep ngay do bsang/chieu/ca ngay thi cộng vô giờ trễ sớm luôn
                                        */
                                       Late = ChamCongConstant.CheckLate(viewModel, cong, ca, miss, nv),
                                       Early = ChamCongConstant.CheckEarly(viewModel, cong, ca, miss, nv),
                                       Des = ChamCongConstant.CheckTimeDown(viewModel, miss, nv, cong.Date)
                                   };
                        if (lstFileChamCong != null)
                        {
                            lstFileChamCong = data.ToList();
                            gridView.ItemsSource = lstFileChamCong;
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do something 
            LoadDataFromMCC();
        }
        // Completed Method 
        void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("Cancelled");
            }
            else if (e.Error != null)
            {
                MessageBox.Show("Exception Thrown");
            }
            else
            {
                if (ChamCongConstant.bIsConnected)
                {
                    ChamCongConstant.axCZKEM1.Disconnect();
                    ChamCongConstant.bIsConnected = false;
                }
            }
            SetLoadingButtuon(btnLoad, false);
        }
        void SetLoadingButtuon(Button btn, bool status = true)
        {
            Application.Current.Dispatcher.BeginInvoke(
              DispatcherPriority.Background,
              new Action(() =>
              {
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndeterminate(btn, status);
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(btn, status);
              }
            ));

        }
        #endregion

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox cbo = (sender as ComboBox);
                ComboBoxItem item = cbo.SelectedItem as ComboBoxItem;
                int month = int.Parse(item.Content.ToString());
                ChamCongConstant.month = month;
                if (DateTime.Now.Month == 1 && month == 12)
                {
                    ChamCongConstant.year = DateTime.Now.Year - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
