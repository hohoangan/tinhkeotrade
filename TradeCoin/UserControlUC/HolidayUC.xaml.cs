﻿using Microsoft.Win32;
using QuanLy.Models;
using QuanLy.ModelViews;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for HolidayUC.xaml
    /// </summary>
    public partial class HolidayUC : UserControl
    {
        ViewModel viewModel;
        public HolidayUC()
        {
            InitializeComponent();
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            Load();
        }

        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion

        #region event
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();
            }
            catch { }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Muốn lưu tất cả thay đổi", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var upadte = viewModel.Holidays.Where(x => x.Id != 0);
                    var insert = viewModel.Holidays.Where(x => x.Id == 0);
                    if (upadte != null)
                    {
                        _dbContext.Holidays.UpdateRange(upadte);
                        _dbContext.SaveChanges();
                    }
                    if (insert != null)
                    {
                        //update
                        _dbContext.Holidays.AddRange(insert);
                        _dbContext.SaveChanges();
                    }
                }
                MessageBox.Show("Lưu thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Save_Click(object sender, MouseButtonEventArgs e)
        {
            Save();
        }

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var del = gridView.SelectedItem as Holiday;
                MessageBoxResult rs = MessageBox.Show("Muốn xóa dòng này", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    context.Holidays.Remove(del);
                    context.SaveChanges();
                }
                MessageBox.Show("Xoá thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region FUNC
        void Save()
        {
            try
            {
                var row = gridView.SelectedItem as Holiday;
                int Id = row?.Id ?? 0;

                if (row == null)
                    return;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    if (Id == 0)
                    {
                        _dbContext.Holidays.Add(row);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        //update
                        _dbContext.Holidays.Update(row);
                        _dbContext.SaveChanges();
                    }
                }

                MessageBox.Show("Lưu thành công");
                ChangeActionOfDatagrid(true);
                Load();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                //gridView.ClearFilters();
                viewModel = new ViewModel();
                DataContext = viewModel;
                gridView.ItemsSource = viewModel.Holidays;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion
    }
}
