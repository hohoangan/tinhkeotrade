﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using System.Reflection;
using Microsoft.Win32;
using System.Windows.Threading;
using MaterialDesignThemes.Wpf;
using System.Text.RegularExpressions;
using System.Linq;
using QuanLy.ModelViews;
using QuanLy.Models;
using System.ComponentModel;
using System.Diagnostics;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for ContractUC.xaml
    /// </summary>
    public partial class ContractUC : UserControl, INotifyPropertyChanged
    {
        string HDmau = "HDLD.doc";
        #region BINDING
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public List<StaffContract> StaffContracts { get; set; }
        public Company Company { get; set; }
        public Shift Shift { get; set; }
        public List<Company> _companies { get; set; }
        public Staff _staff { get; set; }
        public StaffContract _staffContract { get; set; }
        public Position _position { get; set; }
        public Staff Staff
        {
            get { return _staff; }
            set
            {
                if (value != _staff)
                {
                    _staff = value;
                    OnPropertyChanged("Staff");
                }
            }
        }
        public List<Company> Companies
        {
            get { return _companies; }
            set
            {
                if (value != _companies)
                {
                    _companies = value;
                    OnPropertyChanged("Companies");
                }
            }
        }
        public StaffContract StaffContract
        {
            get { return _staffContract; }
            set
            {
                if (value != _staffContract)
                {
                    _staffContract = value;
                    OnPropertyChanged("StaffContract");
                }
            }
        }
        public Position Position
        {
            get { return _position; }
            set
            {
                if (value != _position)
                {
                    _position = value;
                    OnPropertyChanged("Position");
                }
            }
        }
        #endregion

        ViewModel viewModel;
        public ContractUC()
        {
            InitializeComponent();
            viewModel = new ViewModel();
            DataContext = this;
            cboName.ItemsSource = viewModel.Staffs;
            LoadImage();
        }

        void LoadImage()
        {
            try
            {
                imgFront.ImageSource = Helps.GetImage("cmnd_front.png");
                imgBack.ImageSource = Helps.GetImage("cmnd_back.png");
            }
            catch { }
        }

        private void Button_Create(object sender, RoutedEventArgs e)
        {
            if (cboContract.SelectedItem == null)
            {
                MessageBox.Show("Chọn [Hợp Đồng] trước khi IN");
                return;
            }

            if(radOFFER_LETTER.IsChecked == true && cboManager.SelectedItem == null)
            {
                MessageBox.Show("Chọn thông tin [Người quản lý] trước khi IN");
                return;
            }

            MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu Hợp đồng", "", MessageBoxButton.OKCancel);
            if (rs.Equals(MessageBoxResult.Cancel))
            {
                return;
            }

            string filename = string.Empty;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Word 97-2003 Document|*.doc|Word Document |*.docx";
            try
            {
                SetLoadingButtuon(btn_HDTV, true);

                string dir = Helps.GetBasePath(HDmau);

                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;
                    CreateWordDocument(dir, filename);
                    //neu la hd tuvan thi kem theo 2 ban
                    if (HDmau.Equals("HD_TUVAN.doc"))
                    {
                        string savePath = System.IO.Path.GetDirectoryName(filename);
                        dir = Helps.GetBasePath("HD_TUVAN_BIENBANXACNHAN.doc");
                        string filenameBBXN = System.IO.Path.Combine(savePath, "BBXN_"+Staff.FullName);
                        CreateWordDocument(dir, filenameBBXN);

                        dir = Helps.GetBasePath("HD_TUVAN_CAMKET02_TNCN.doc");
                        string filenameCAMKET02 = System.IO.Path.Combine(savePath, "CAMKET02_TNCN_" + Staff.FullName);
                        CreateWordDocument(dir, filenameCAMKET02);
                    }
                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process wordProcess = new Process();
                        wordProcess.StartInfo.FileName = filename;
                        wordProcess.StartInfo.UseShellExecute = true;
                        wordProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex?.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                SetLoadingButtuon(btn_HDTV, false);
            }
        }

        private void ClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
        }

        void SetLoadingButtuon(Button btn, bool status = true)
        {
            Application.Current.Dispatcher.BeginInvoke(
              DispatcherPriority.Background,
              new Action(() =>
              {
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndeterminate(btn, status);
                  MaterialDesignThemes.Wpf.ButtonProgressAssist.SetIsIndicatorVisible(btn, status);
              }
            ));
            
        }

        //Creeate the Doc Method
        private void CreateWordDocument(object filename, object SaveAs)
        {
            Word.Application wordApp = new Word.Application();
            object missing = Missing.Value;
            Word.Document myWordDoc = null;
            try
            {
                if (File.Exists((string)filename))
                {
                    object readOnly = false;
                    object isVisible = false;
                    wordApp.Visible = false;

                    myWordDoc = wordApp.Documents.Open(ref filename, ref missing, ref readOnly,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing,
                                            ref missing, ref missing, ref missing, ref missing);
                    myWordDoc.Activate();

                    //find and replace
                    //company
                    Helps.FindAndReplace(wordApp, "<Company.Name>", Company.Name);
                    Helps.FindAndReplace(wordApp, "<Company.Code>", Company.Code);
                    Helps.FindAndReplace(wordApp, "<Company.Address>", Company.Address);
                    Helps.FindAndReplace(wordApp, "<Company.Phone>", Company.Phone);
                    Helps.FindAndReplace(wordApp, "<Company.TaxNo>", Company.TaxNo);
                    Helps.FindAndReplace(wordApp, "<Company.Representative>", Company.Representative.ToUpper());
                    Helps.FindAndReplace(wordApp, "<Company.Position>", Company.Position);
                    //nhan vien
                    Helps.FindAndReplace(wordApp, "<Staff.FullName>", Staff.FullName.ToUpper());
                    Helps.FindAndReplace(wordApp, "<Staff.BirthDay>", Staff.BirthDay.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<Staff.Country>", FirstCharToUpper(Staff.Country));
                    Helps.FindAndReplace(wordApp, "<Staff.AddressNow>", FirstCharToUpper(Staff.AddressNow));
                    Helps.FindAndReplace(wordApp, "<Staff.Address>", Staff.Address);
                    if (HDmau.Equals("HD_TUVAN.doc"))
                    {
                        string masothue = Staff.TaxNo?.ToString();
                        
                        if (masothue!= null && !string.Empty.Equals(masothue))
                        {
                            for (int i = 0; i < masothue.Length; i++)
                            {
                                Helps.FindAndReplace(wordApp, "<"+i+">", masothue[i].ToString() ?? "");
                            }
                            for (int i = masothue.Length; i < 13; i++)
                            {
                                Helps.FindAndReplace(wordApp, "<" + i + ">", "");
                            }
                        }
                        else
                        {
                            for (int i = 1; i < 13; i++)
                            {
                                Helps.FindAndReplace(wordApp, "<" + i + ">", "");
                            }
                        }

                        string sotienbangchu = ConvertMoneyToWords.ChuyenSoSangChuoi(StaffContract.BasicSalary);
                        Helps.FindAndReplace(wordApp, "<Money2Words>", sotienbangchu.Trim());
                    }
                    Helps.FindAndReplace(wordApp, "<Staff.CMND>", Staff.CMND);
                    Helps.FindAndReplace(wordApp, "<Staff.Email>", Staff.Email);
                    Helps.FindAndReplace(wordApp, "<Staff.EmailPrivate>", Staff.EmailPrivate);
                    Helps.FindAndReplace(wordApp, "<Staff.Phone>", Staff.Phone);
                    Helps.FindAndReplace(wordApp, "<Staff.CMNDCreatedDate>", Staff.CMNDCreatedDate.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<Staff.CMNDAdress>", FirstCharToUpper(Staff.CMNDAdress));
                    Helps.FindAndReplace(wordApp, "<Staff.BHXH>", Staff.BHXH);
                    Helps.FindAndReplace(wordApp, "<ManagerName>", cboManager.Text);
                    Helps.FindAndReplace(wordApp, "<Shift.Start>", string.Format("{0}g{1}", Shift?.Start.Hours, Shift?.Start.Minutes));
                    Helps.FindAndReplace(wordApp, "<Shift.End>", string.Format("{0}g{1}", Shift?.End.Hours, Shift?.End.Minutes));
                    //Hop dong
                    Helps.FindAndReplace(wordApp, "<StaffContract.ContractNo>", StaffContract.ContractNo);
                    Helps.FindAndReplace(wordApp, "<StaffContract.MonthContract>", StaffContract.MonthContract);
                    Helps.FindAndReplace(wordApp, "<StaffContract.FromDate>", StaffContract.FromDate.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<StaffContract.ToDate>", StaffContract.ToDate?.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<Position.Department.Name>", Position.Department.Name);
                    Helps.FindAndReplace(wordApp, "<Position.Name>", Position.Name);
                    Helps.FindAndReplace(wordApp, "<StaffContract.SignatureDate>", StaffContract.SignatureDate.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<StaffContract.BasicSalary>", StaffContract.BasicSalary.ToString("###,##0").Replace(",", "."));
                    Helps.FindAndReplace(wordApp, "<StaffContract.BasicSalaryDiv2>", (StaffContract.BasicSalary/2).ToString("###,##0").Replace(",", "."));
                    Helps.FindAndReplace(wordApp, "<DateNow>", DateTime.Now.ToString("dd/MM/yyyy"));
                    Helps.FindAndReplace(wordApp, "<YearNow>", DateTime.Now.Year);
                    //salary
                    string lunch = string.Empty, gas = string.Empty, phone = string.Empty, bonus = string.Empty;
                    if (StaffContract.AllowanceLunch > 0)
                        lunch = StaffContract.AllowanceLunch.ToString("###,##0").Replace(",", ".") + " VNĐ/tháng";
                    Helps.FindAndReplace(wordApp, "<StaffContract.AllowanceLunch>", lunch);

                    if (StaffContract.AllowanceGas > 0)
                        gas = StaffContract.AllowanceGas.ToString("###,##0").Replace(",", ".") + " VNĐ/tháng";
                    Helps.FindAndReplace(wordApp, "<StaffContract.AllowanceGas>", gas);

                    if (StaffContract.AllowancePhone > 0)
                        phone = StaffContract.AllowancePhone.ToString("###,##0").Replace(",", ".") + " VNĐ/tháng";
                    Helps.FindAndReplace(wordApp, "<StaffContract.AllowancePhone>", phone);

                    if (StaffContract.Bonus > 0)
                        bonus = StaffContract.Bonus.ToString("###,##0").Replace(",", ".") + " VNĐ/tháng";
                    Helps.FindAndReplace(wordApp, "<StaffContract.Bonus>", bonus);
                    //ngay ky
                    string dt = StaffContract.SignatureDate.ToString("dd/MM/yyyy");
                    Helps.FindAndReplace(wordApp, "<dd>", dt.Split("/")[0]);
                    Helps.FindAndReplace(wordApp, "<mm>", dt.Split("/")[1]);
                    Helps.FindAndReplace(wordApp, "<yyyy>", dt.Split("/")[2]);

                    //Save as
                    myWordDoc.SaveAs2(ref SaveAs, ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing,
                                    ref missing, ref missing, ref missing);
                }
                else
                {
                    MessageBox.Show("Không tìm thấy file mẫu hợp đồng!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
            finally
            {
                myWordDoc.Close();
                wordApp.Quit();
            }
        }

        #region format currency (money)
        private void txtsalary_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox txt = sender as TextBox;
                txt.Text = string.Format("{0:#,##0}", double.Parse(txt.Text));
            }
            catch (Exception)
            {
            }
        }

        private void txtsalary_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            txt.SelectionStart = txt.Text.Length;
            txt.SelectionLength = 0;
        }

        private static readonly Regex _regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }

        // Use the PreviewTextInputHandler to respond to key presses 
        private void PreviewTextInputHandler(Object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        // Use the DataObject.Pasting Handler  
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }
        #endregion

        #region Data change
        private void Flipper_OnIsFlippedChanged(object sender, RoutedPropertyChangedEventArgs<bool> e)
        {

        }

        void ChangedData(object sender, TextChangedEventArgs e)
        {
            return;
            try
            {
                lbname.Content = cboName.DisplayMemberPath.ToUpper();
                lbCMND.Content = txtcmnd.Text;
                lbBirthDay.Content = txtBirthday.Text;
                lbAddress.Text = FirstCharToUpper(txtaddress.Text);
                lbCountry.Content = FirstCharToUpper(txtcountry.Text);

                DateTime dt = Convert.ToDateTime(txtcreatedate_cmnd.Text);
                lbday.Content = dt.Day;
                lbmonth.Content = dt.Month;
                lbyear.Content = dt.Year;
                lbaddress_cmnd.Content = FirstCharToUpper(txtaddress_cmnd.Text);
            }
            catch (Exception)
            {
            }
        }
        public static string FirstCharToUpper(string input)
        {
            try
            {
                return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(input.ToLower());
            }
            catch (Exception)
            {
                return input;
            }
        }

        private void Datetime_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangedData(null, null);
        }
        #endregion

        #region event
        private void txtName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var staff = cboName.SelectedItem as Staff;
                Staff = staff;
                using (AppDbContext context = new AppDbContext())
                {
                    StaffContracts = context.StaffContracts.Where(x => x.StaffId.Equals(staff.Id)).ToList();
                    Companies = (from c in context.Companies.ToList()
                                 join sc in StaffContracts.ToList()
                                on c.Id equals sc.CompanyId
                                select c).ToList();
                    Shift = context.Shifts.Where(x => x.Id == Staff.ShiftId).FirstOrDefault();
                }
                cboContract.ItemsSource = StaffContracts;
                cboCompanies.ItemsSource = Companies.Distinct();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
           
        }

        private void cboContract_SelectionChanged(object sender, SelectionChangedEventArgs e)
        { 
            try
            {
                var contract = cboContract.SelectedItem as StaffContract;
                StaffContract = contract;
                if (contract == null)
                {
                    return;
                }
                changeRadioHopDong(contract.ContractType);
                using (AppDbContext context = new AppDbContext())
                {
                    Position = (from a in context.Positions.Where(x=>x.Id == contract.PositionId)
                                 join b in context.Departments
                                 on a.DepartmentId equals b.Id
                                 select new Position
                                 {
                                     Id = a.Id,
                                     Name = a.Name,
                                     DepartmentId = a.DepartmentId,
                                     Department = b
                                 }).FirstOrDefault();

                    Company = context.Companies.Where(x => x.Id == contract.CompanyId).FirstOrDefault();

                    cboManager.ItemsSource = (from m in context.Managers.Where(x => x.CompanyId == Company.Id && Position.Department.Id == x.DepartmentId)
                                              join s in context.Staffs
                                              on m.StaffId equals s.Id
                                              select s).ToList();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        void changeRadioHopDong(string ContractName)
        {
            try
            {
                if (ContractName.Equals("HĐTV"))
                {
                    radHDTV.IsChecked = true;
                    HDmau = "HDTV.doc";
                   
                }
                else if (ContractName.Equals("HĐLĐ") || ContractName.Equals("HĐLĐ NoLtd"))
                {
                    radHDLD.IsChecked = true;
                    HDmau = "HDLD.doc";
                }
                else if (ContractName.Equals("HĐLĐ Limited"))
                {
                    radHDLD_Limited.IsChecked = true;
                    HDmau = "HDLD_Limited.doc";
                }
                else if (ContractName.Equals("HĐTV TECH"))
                {
                    radHDTV_TECH.IsChecked = true;
                    HDmau = "HDTV_TECH.doc";
                }
                else if (ContractName.Equals("HĐLĐ TECH"))
                {
                    radHDLD_TECH.IsChecked = true;
                    HDmau = "HDLD_TECH.doc";
                }
                else if (ContractName.Equals("HĐ TUVAN"))
                {
                    radHD_TUVAN.IsChecked = true;
                    HDmau = "HD_TUVAN.doc";
                }
                else if (ContractName.Equals("Offer Letter"))
                {
                    radOFFER_LETTER.IsChecked = true;
                    HDmau = "OFFER_LETTER.doc";
                }
            }
            catch (Exception)
            {
            }
        }

        private void cboCompanies_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var com = cboCompanies.SelectedItem as Company;
                if (com == null)
                {
                    cboManager.ItemsSource = null;
                    return;
                }
                cboContract.ItemsSource = StaffContracts.Where(x => x.CompanyId == com.Id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion
    }
}
