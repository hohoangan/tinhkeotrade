﻿using QuanLy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for PopupAddMission.xaml
    /// </summary>
    public partial class PopupAddMission : Window
    {
        Mission mission;
        // Define delegate
        public delegate void PassControl(object sender);
        // Create instance (null)
        public PassControl passControl;
        public PopupAddMission()
        {
            InitializeComponent();
        }

        public PopupAddMission(Mission mission)
        {
            InitializeComponent();
            this.mission = mission;
            if (mission.Time.Contains("Cả ngày") == true)//cả ngày
            {
                timepicker.SelectedTime = (new DateTime()).AddTicks(new TimeSpan(8, 0, 0).Ticks);
                timepicker.IsEnabled = false;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(timepicker.SelectedTime == null)
                {
                    MessageBox.Show("Nhập thời gian");
                    timepicker.Focus();
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    mission.ApprovedBy = txtApprovalBy.Text.Trim();
                    mission.TotalHours = timepicker.SelectedTime.Value.TimeOfDay;
                    _dbContext.Missions.Add(mission);
                    _dbContext.SaveChanges();
                    MessageBox.Show("Thêm thành công");
                    if (passControl != null)
                    {
                        passControl(mission);
                    };
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void PresetTimePicker_SelectedTimeChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {

        }
    }
}
