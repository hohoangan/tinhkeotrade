﻿using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using QuanLy.Models;
using QuanLy.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System;
using System.Linq;
using System.Windows;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using LiveCharts.Configurations;
using Separator = LiveCharts.Wpf.Separator;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for ReviewCandidateUC.xaml
    /// </summary>
    public partial class ReviewCandidateUC : UserControl
    {
        ViewModel viewModel;
        public SeriesCollection SeriesCollection { get; set; }
        public ReviewCandidateUC()
        {
            InitializeComponent();
            Load();
        }

        void Load()
        {
            try
            {
                viewModel = new ViewModel();
                DataContext = viewModel;
                cboUV.ItemsSource = viewModel.Candidates;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        Button CreateButton(string name, int Id, SolidColorBrush color, bool brush = false, bool rightClick = false)
        {
            try
            {
                Button txt = new Button();
                txt.HorizontalAlignment = HorizontalAlignment.Left;
                txt.Margin = new Thickness(10);
                txt.Content = name;
                int len = name.Length * 12;
                //txt.Width = len < 80 ? 80 : len;
                txt.Tag = Id;
                txt.Foreground = Brushes.Black;
                if (brush)
                {
                    txt.Background = color;
                    txt.BorderBrush = color;
                }
                //tao menucontext
                if (rightClick)
                {
                    txt.ToolTip = "Click chuột phải để thêm trạng thái";
                    ContextMenu contextMenu = new ContextMenu();
                    var status = viewModel.CandidateStatuses.Where(x => x.CandidateStepId == Id).ToList();
                    status.ForEach(item =>
                    {
                        MenuItem item1 = new MenuItem();
                        item1.Header = item.Name;
                        item1.Tag = item.Id;//statusId
                        item1.ToolTip = txt.Tag;//stepId
                        item1.Click += Item1_Click;
                        contextMenu.Items.Add(item1);
                    });
                    //thêm nút xóa
                    MenuItem item1 = new MenuItem();
                    item1.Header = "Xóa";
                    item1.Tag = Id;//stepId
                    item1.Click += ItemDEL_Click; 
                    contextMenu.Items.Add(item1);

                    txt.ContextMenu = contextMenu;
                }
                return txt;
            }
            catch (Exception)
            {
                return new Button();
            }
        }

        private void ItemDEL_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuItem itemStatus = sender as MenuItem;
                Candidate uv = cboUV.SelectedItem as Candidate;
                using (AppDbContext context = new AppDbContext())
                {
                    var pro = context.CandidateProcesses.Where(x => x.CandidateId == uv.Id && x.CandidateStepId >= (int)itemStatus.Tag);
                    context.CandidateProcesses.RemoveRange(pro);
                    context.SaveChanges();
                    viewModel = new ViewModel();
                    UpdateProcess();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Item1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MenuItem itemStatus = sender as MenuItem;
                Candidate uv = cboUV.SelectedItem as Candidate;
                //them status cho step
                using (AppDbContext context = new AppDbContext())
                {
                    var newStep = new CandidateProcess
                    {
                        Date = DateTime.Now.Date,
                        CandidateId = uv.Id,
                        CandidateStepId = (int)itemStatus.ToolTip,// stepId
                        CandidateStatusId = (int)itemStatus.Tag// statusID
                    };
                    //ktra neu co step roi thi update status || them moi steps + status
                    var pro = context.CandidateProcesses.Where(x => x.CandidateStepId == newStep.CandidateStepId
                                                                    && x.CandidateId == newStep.CandidateId).FirstOrDefault();
                    if (pro != null)
                    { 
                        //update
                        pro.CandidateStatusId = newStep.CandidateStatusId;
                        pro.Date = DateTime.Now.Date;
                    }//insert
                    else
                    {
                        var stepPrev = context.CandidateSteps.Where(x => x.Id == newStep.CandidateStepId).FirstOrDefault();

                        //ktra step trước đã có chưa
                        var prev = context.CandidateProcesses.Where(x => x.CandidateStepId == stepPrev.ParentId
                                                                    && x.CandidateId == newStep.CandidateId).FirstOrDefault();
                        //neu parnet = null hoac da co step trước đó thì cho tạo
                        if (stepPrev.ParentId == null || prev != null)
                            context.CandidateProcesses.Add(newStep);
                    }
                    context.SaveChanges();
                    viewModel = new ViewModel();
                    UpdateProcess();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void txtName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                //Process
                UpdateProcess();
                //Chart
                SeriesCollection = new SeriesCollection();
                Candidate uv = cboUV.SelectedItem as Candidate;
                var scoreUV = (from cq in viewModel.CandidateQuestions.Where(x => x.CandidateId == uv.Id)
                               join q in viewModel.Questions on cq.QuestionId equals q.Id
                               join qt in viewModel.QuestionTypes on q.QuestionTypeId equals qt.Id
                               group new { cq, q, qt } by new { qt } into g
                               select new
                               {
                                   TotalQues = g.Count(x => x.cq.Id != 0),
                                   QuestionTypeName = g.Key.qt.Name,
                                   AvgScore = g.Average(x => x.cq.Scores)
                               }
                              ).ToList();
                lbAVF.Content = "";
                if (scoreUV.Count != 0)
                {
                    lbAVF.Content = Math.Round(scoreUV.Average(x => x.AvgScore),2);
                }

                scoreUV?.ForEach(item =>
                {
                    SeriesCollection.Add(new PieSeries
                    {
                        Title = string.Format("{0} ({1} câu)", item.QuestionTypeName, item.TotalQues),
                        Values = new ChartValues<ObservableValue> { new ObservableValue(item.AvgScore) },
                        DataLabels = true,
                        FontSize = 15,
                        LabelPoint = point => point.Y+"đ" //value hiển thị trong chart
                    }) ;
                });
                Chart.Series = SeriesCollection;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        void UpdateProcess()
        {
            stackPanelStep.Children.Clear();
            //Process
            Candidate Candidate = cboUV.SelectedItem as Candidate;
            var pro = (from step in viewModel.CandidateSteps.ToList()
                       join process in viewModel.CandidateProcesses.Where(x => x.CandidateId == Candidate.Id).ToList() on step.Id equals process.CandidateStepId into processG
                       from process in processG.DefaultIfEmpty()
                       select new
                       {
                           step,
                           process
                       }).ToList();

            pro?.ForEach(item =>
            {
                WrapPanel stc = new WrapPanel();
                stc.Orientation = Orientation.Horizontal;

                SolidColorBrush color = new SolidColorBrush();
                bool brush = false;
                //neu chua process
                if (item.process == null)
                {
                    color = Brushes.LightGray;
                    brush = true;
                }
                //ADD STEP
                Button txt = CreateButton(item.step.Name, item.step.Id, color, brush, true);
                stc.Children.Add(txt);
                //ADD STATUS OF STEP
                if (item.process != null)
                {
                    color = Brushes.Cyan;
                    //da process thi lay status hien tai
                    var status = viewModel.CandidateStatuses.Where(x => x.Id == item.process.CandidateStatusId).FirstOrDefault();
                    Button btnStatus = CreateButton(status.Name, status.Id, color, true);
                    stc.Children.Add(btnStatus);
                    //add date
                    color = Brushes.Lime;
                    Button btnDate = CreateButton(item.process.Date.Date.ToString("dd/MM/yy"),0, color, true);
                    stc.Children.Add(btnDate);
                    //add lý do
                    TextBox tb = new TextBox();
                    tb.Style = (Style)Application.Current.Resources["MaterialDesignFloatingHintTextBox"];
                    tb.Width = 200;
                    tb.Tag = item.process.Id;//Id cua candidateprocess
                    tb.Text = item.process.Des;
                    tb.TextChanged += Tb_TextChanged;
                    stc.Children.Add(tb);
                }

                stackPanelStep.Children.Add(stc);
                scrollStep.ScrollToEnd();
            });
        }

        private void Tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox tb = sender as TextBox;
                //Update status
                using (AppDbContext context = new AppDbContext())
                {
                    var pro = context.CandidateProcesses.Where(x => x.Id == (int)tb.Tag).FirstOrDefault();
                    pro.Des = tb.Text.Trim();
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
