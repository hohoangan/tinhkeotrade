﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QuanLy.Models;
using System.Linq;
using Syncfusion.UI.Xaml.Grid;
using System.Collections.ObjectModel;
using System.Data;
using QuanLy.ModelViews;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Data.Extensions;
using System.Windows.Media.Animation;
using System.ComponentModel;
using Syncfusion.UI.Xaml.Grid.Converter;
using Microsoft.Win32;
using System.Diagnostics;
using Syncfusion.XlsIO;
using System.Text.RegularExpressions;

namespace QuanLy.UserControlUC
{
    /// <summary>
    /// Interaction logic for StaffUC.xaml
    /// </summary>
    public partial class TradeCoinUC : UserControl
    {
        ViewModel viewModel;
        public TradeCoinUC()
        {
            InitializeComponent();
            //delete cell
            this.gridView.SelectionController = new GridSelectionControllerExt(gridView);
            gridView.Height = Constant.windowHeight;
            txttodate.SelectedDate = DateTime.Now.Date;
            Load();
        }

        #region GRID
        void ChangeActionOfDatagrid(bool allow = true)
        {
            gridView.AllowFiltering = allow;
            gridView.AllowSorting = allow;
        }
        #endregion

        #region EVENT
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();
            }
            catch { }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Load();
        }

        private void btnSaveAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult rs = MessageBox.Show("Muốn lưu tất cả thay đổi", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var upadte = viewModel.CoinTrades.Where(x => x.Id != 0);
                    var insert = viewModel.CoinTrades.Where(x => x.Id == 0 && !string.IsNullOrEmpty(x.CoinName));
                    if (upadte != null)
                    {
                        _dbContext.CoinTrades.UpdateRange(upadte);
                        _dbContext.SaveChanges();
                    }
                    if (insert != null)
                    {
                        //update
                        _dbContext.CoinTrades.AddRange(insert.Select(x=> { x.Date = DateTime.Now.Date; return x; }));
                        _dbContext.SaveChanges();
                    }
                }
                MessageBox.Show("Lưu thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void Save_Click(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var cointrade = gridView.SelectedItem as CoinTrade;
                int Id = cointrade?.Id ?? 0;
                if (cointrade == null)
                    return;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    if (Id == 0)
                    {
                        cointrade.Date = DateTime.Now.Date;
                        _dbContext.CoinTrades.Add(cointrade);
                        _dbContext.SaveChanges();
                    }
                    else
                    {
                        //update
                        _dbContext.CoinTrades.Update(cointrade);
                        _dbContext.SaveChanges();
                    }
                }

                MessageBox.Show("Lưu thành công");
                ChangeActionOfDatagrid(true);
                Load();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            XuatExcel();
        }
        void XuatExcel()
        {
            try
            {
                if (viewModel?.CoinTrades.Count == 0 || gridView.ItemsSource == null)
                    return;
                MessageBoxResult rs = MessageBox.Show("Chọn nơi lưu file", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }

                string filename = string.Empty;
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook |*.xlsx";
                if (saveFileDialog.ShowDialog() == true)
                {
                    filename = saveFileDialog.FileName;
                    var options = new ExcelExportingOptions();
                    //chọn bỏ cột khi xuất
                    options.ExcludeColumns.Add("Save");
                    options.ExcludeColumns.Add("Del");
                    options.ExcelVersion = ExcelVersion.Excel97to2003;
                    //options.CellsExportingEventHandler = CellExportingHandler;
                    var excelEngine = gridView.ExportToExcel(gridView.View, options);
                    var workBook = excelEngine.Excel.Workbooks[0];
                    workBook.SaveAs(filename);
                    MessageBoxResult rsOK = MessageBox.Show("Đã tạo file thành công!\nCó muốn mở file vừa xuất?", "", MessageBoxButton.OKCancel);
                    if (rsOK.Equals(MessageBoxResult.OK))
                    {
                        Process xlsProcess = new Process();
                        xlsProcess.StartInfo.FileName = filename;
                        xlsProcess.StartInfo.UseShellExecute = true;
                        xlsProcess.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        #region FUNC
        void Load()
        {
            ChangeActionOfDatagrid(true);
            try
            {
                viewModel = new ViewModel();
                if (txttodate.SelectedDate != null)
                    viewModel.LoadCoinTrade(txttodate.SelectedDate.Value);
                DataContext = viewModel;
                gridView.ItemsSource = viewModel.CoinTrades;

                double loinhuan = viewModel.CoinTrades.Sum(x => x.TP1 + x.TP2);
                double taisan = double.Parse(txtTaisan.Text.Equals(string.Empty) ? "1" : txtTaisan.Text.Replace(".", ",").ToString());
                double percent = (loinhuan / taisan * 100);
                txtLoinhuan.Text = string.Format("{0} $ ({1})%", Math.Round(loinhuan,2).ToString().Replace(",", "."), 
                    Math.Round(percent, 2).ToString().Replace(",", "."));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }
        #endregion

        private void Del_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var del = gridView.SelectedItem as CoinTrade;
                MessageBoxResult rs = MessageBox.Show("Muốn xóa dòng này", "", MessageBoxButton.OKCancel);
                if (rs.Equals(MessageBoxResult.Cancel))
                {
                    return;
                }
                using (AppDbContext context = new AppDbContext())
                {
                    context.CoinTrades.Remove(del);
                    context.SaveChanges();
                }
                MessageBox.Show("Xoá thành công");
                Load();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void btnCal_Click(object sender, RoutedEventArgs e)
        {
            if (panelCalculator.Visibility == Visibility.Collapsed)
            {
                panelCalculator.Visibility = Visibility.Visible;
                panelCalculatorButton.Visibility = Visibility.Visible;
            }
            else
            {
                panelCalculator.Visibility = Visibility.Collapsed;
                panelCalculatorButton.Visibility = Visibility.Collapsed;
            }
        }

        #region nhap so
        private void txtTotalDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox txt = sender as TextBox;
                txt.Text = txt.Text;
                //tinh size coin de mua
                double taisan = double.Parse(txtTaisan.Text.ToString());
                double risk = double.Parse(txtRisk.Text.Equals(string.Empty) ? "0" : txtRisk.Text.Replace(".", ",").ToString());
                double entry = double.Parse(txtentry.Text.Equals(string.Empty) ? "0" : txtentry.Text.Replace(".", ",").ToString());
                double sl = double.Parse(txtSL.Text.Equals(string.Empty) ? "0" : txtSL.Text.Replace(".", ",").ToString());
                double tp = double.Parse(txtTP.Text.Equals(string.Empty) ? "0" : txtTP.Text.Replace(".", ",").ToString());
                double leverage = double.Parse(txtLeverage.Text.Equals(string.Empty) ? "1" : txtLeverage.Text.ToString());

                double riskcost = (taisan * risk / 100);
                double size = riskcost / Math.Abs(entry - sl);
                double sizecost = size * entry;
                double margin = sizecost / leverage;
                txtPositionSize.Text = Math.Round(size, 5).ToString() + " COIN";
                txtPositionSizeValue.Text = Math.Round(sizecost, 5).ToString() + " $";
                txtMargin.Text = Math.Round(margin, 5).ToString() + " $";
                txtMargin.Foreground = margin >= riskcost ? Brushes.LightGreen : Brushes.OrangeRed;
                //tinh ti le RR
                double rr = Math.Round(Math.Abs(tp - entry) / Math.Abs(entry - sl), 1);
                lbRR.Content = string.Format("1R:{0}R", rr);
                lbRR.Foreground = rr >= 1 ? Brushes.LightGreen : Brushes.OrangeRed;
            }
            catch (Exception)
            {
            }
        }
        private static readonly Regex _regex = new Regex("[^0-9.-]+"); //regex that matches disallowed text
        private static bool IsTextAllowed(string text)
        {
            return !_regex.IsMatch(text);
        }
        private void PreviewTextInputHandler(Object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        // Use the DataObject.Pasting Handler  
        private void PastingHandler(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!IsTextAllowed(text)) e.CancelCommand();
            }
            else e.CancelCommand();
        }

        private void txtTotalDate_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            txt.SelectionStart = txt.Text.Length;
            txt.SelectionLength = 0;
        }
        #endregion

        private void btnSaveTrade_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double taisan = double.Parse(txtTaisan.Text.ToString());
                double risk = double.Parse(txtRisk.Text.Equals(string.Empty) ? "0" : txtRisk.Text.Replace(".", ",").ToString());
                double entry = double.Parse(txtentry.Text.Equals(string.Empty) ? "0" : txtentry.Text.Replace(".", ",").ToString());
                double sl = double.Parse(txtSL.Text.Equals(string.Empty) ? "0" : txtSL.Text.Replace(".", ",").ToString());
                double tp = double.Parse(txtTP.Text.Equals(string.Empty) ? "0" : txtTP.Text.Replace(".", ",").ToString());
                double leverage = double.Parse(txtLeverage.Text.Equals(string.Empty) ? "1" : txtLeverage.Text.ToString());
                string des = txtDes.Text.ToString();
                double riskcost = (taisan * risk / 100);
                double size = riskcost / Math.Abs(entry - sl);
                double sizecost = size * entry;
                double margin = sizecost / leverage;

                using (AppDbContext _dbContext = new AppDbContext())
                {
                    var cointrade = new CoinTrade
                    {
                        CoinName = "XXX",
                        Date = DateTime.Now.Date,
                        Descriptions = des,
                        Entry = entry,
                        TakeProfit = tp,
                        Id = 0,
                        Leverage = (int)leverage,
                        MarginCost = margin,
                        PositionSize = size,
                        Stoploss = sl
                    };
                    _dbContext.CoinTrades.Add(cointrade);
                    _dbContext.SaveChanges();
                }
                Load();
            }
            catch (Exception)
            {
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtentry.Text = "";
            txtSL.Text = "";
            txtTP.Text = "";
            txtPositionSize.Text = "";
            txtPositionSizeValue.Text = "";
            txtLeverage.Text = "";
            txtDes.Text = "";
            txtMargin.Text = "";
            lbRR.Content = "";
        }

        private void txtTaisan_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!txtTaisan.Text.Equals(string.Empty))
            {
                double taisan = double.Parse(txtTaisan.Text.ToString());
                double risk = double.Parse(txtRisk.Text.ToString().Equals(string.Empty) ? "0" : txtRisk.Text.ToString());
                txtcostrisk.Text = (taisan * risk / 100).ToString() + " $";
            }
        }

        private void Copy_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var record = this.gridView.View.Records[this.gridView.SelectedIndex].Data as CoinTrade;
                var coin = record;
                coin.Id = 0;
                coin.CoinName = coin.CoinName + " - Copy";
                coin.TP1 = 0;
                coin.TP2 = 0;
                ChangeActionOfDatagrid(false);
                this.gridView.View?.AddNew();
                this.gridView.View?.CommitNew();

                this.gridView.View.Records[0].Data = coin;
            }
            catch { }
        }
    }
}
