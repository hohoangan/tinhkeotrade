﻿using QuanLy.Models;
using QuanLy.ModelViews;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace QuanLy.Extensions
{
    /// <summary>
    /// Implementation class for ItemsSourceSelector interface
    /// </summary>
    public class ItemsSourceSelector : IItemsSourceSelector
    {
        public IEnumerable GetItemsSource(object record, object dataContext)
        {
            if (record == null)
                return null;

            var candidateQuestion = record as CandidateQuestion;
            int candidateId = candidateQuestion.CandidateId;

            var viewModel = dataContext as ViewModel;
            int positionId = viewModel.Candidates.Where(x=>x.Id == candidateId).Select(x=>x.PositionId).FirstOrDefault();
            //lay ra danh sach
            if (positionId > 0)
            {
                var ques = (from qp in viewModel.QuestionForPositions.Where(x => x.PositionId == positionId)
                           join q in viewModel.Questions
                           on qp.QuestionId equals q.Id
                           select q).ToList();
                return ques;
            }
            //ko co thong tin ko load
            return null;//viewModel.Questions.ToList();
        }
    }
}
