﻿using DocumentFormat.OpenXml.Drawing;
using Microsoft.Win32;
using QuanLy.Extensions;
using QuanLy.Models;
using QuanLy.ModelViews;
using Syncfusion.UI.Xaml.Grid.Converter;
using Syncfusion.XlsIO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace QuanLy.Extensions
{
    public static class ChamCongConstant
    {
        public static string IPmcc = "192.168.1.201";
        public static int Port = 4370;
        public static bool bIsConnected = false;
        public static zkemkeeper.CZKEM axCZKEM1;
        public static int month = DateTime.Now.Month;
        public static int year = DateTime.Now.Year;
        public static string CheckTimeDown(ViewModel viewModel, Mission miss, Staff nv, DateTime Date)
        {
            try
            {
                var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id)
                                              .Where(x => x.FromDate <= Date && Date <= x.ToDate).FirstOrDefault();
                string ss = "";
                if (down != null)
                    ss += string.Format("{0}: {1}", down.Reason, down.Time);

                ss += miss != null ? string.Format(" | {0} {1}: {2}", miss.Reason, miss.Time, miss.TotalHours) : "";
                return ss;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static TimeSpan CheckLate(ViewModel viewModel, FileChamCong cong, Shift ca, Mission miss, Staff nv)
        {
            try
            {
                //neu la ngay le thi bo qua
                var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(cong.Date.Date)).FirstOrDefault();
                if (holiday != null)
                    return new TimeSpan();
                if (cong.DayOfWeek.Equals(DayOfWeek.Sunday))
                    return new TimeSpan();

                //check downtime
                var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id && !x.Time.Equals("Chiều"))
                                              .Where(x => x.FromDate <= cong.Date && cong.Date <= x.ToDate).FirstOrDefault();
                if (down != null)
                    return new TimeSpan();

                //cong tac
                TimeSpan congtac = (!miss?.Time.Equals("Chiều") ?? false)
                                    ? (miss.TotalHours > new TimeSpan(4, 0, 0) ? new TimeSpan(4, 0, 0) : miss.TotalHours)
                                    : new TimeSpan();

                //neu thoi gian cong tac >=4h thi cho phep nghi luon
                if (congtac >= new TimeSpan(4, 0, 0))
                    return new TimeSpan();

                //miss punch
                if (cong.Start.Equals(new TimeSpan()))
                    return new TimeSpan(4, 0, 0);

                TimeSpan tresom = ca.Start < cong.Start ? cong.Start - ca.Start : new TimeSpan();
                TimeSpan kq = tresom - congtac;

                if (kq > new TimeSpan(4, 0, 0))
                    return new TimeSpan(4, 0, 0);
                else if (kq < new TimeSpan())
                    return new TimeSpan();
                return kq;
            }
            catch (Exception)
            {
                return new TimeSpan();
            }
        }
        public static TimeSpan CheckEarly(ViewModel viewModel, FileChamCong cong, Shift ca, Mission miss, Staff nv)
        {
            try
            {
                //neu la ngay le thi bo qua
                var holiday = viewModel.Holidays.Where(x => x.Date.Date.Equals(cong.Date.Date)).FirstOrDefault();
                if (holiday != null)
                    return new TimeSpan();

                TimeSpan End = ca.End;
                if (cong.DayOfWeek.Equals(DayOfWeek.Sunday))
                    return new TimeSpan();

                //check downtime
                var down = viewModel.DownTimes.Where(x => x.StaffId == nv.Id && !x.Time.Equals("Sáng"))
                                              .Where(x => x.FromDate <= cong.Date && cong.Date <= x.ToDate).FirstOrDefault();
                if (down != null)
                    return new TimeSpan();

                if (cong.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    //neu ca tu 8h thi tinh gio ra la 12AM
                    //neu ca tu 9h thi tinh gio ra la 13PM
                    End = ca.Sat;
                    if (cong.Start.Equals(new TimeSpan()))
                        return new TimeSpan();
                }
                //cong tac
                TimeSpan congtac = (!miss?.Time.Equals("Sáng") ?? false)
                                    ? (miss.TotalHours > new TimeSpan(4, 0, 0) ? new TimeSpan(4, 0, 0) : miss.TotalHours)
                                    : new TimeSpan();
                //neu ngay cong tac >=4h thi cho phep nghi luon
                if (congtac >= new TimeSpan(4, 0, 0))
                    return new TimeSpan();

                //miss punch
                if (cong.End.Equals(new TimeSpan()))
                    return new TimeSpan(4, 0, 0);

                TimeSpan tresom = End > cong.End ? End - cong.End : new TimeSpan();
                TimeSpan kq = tresom - congtac;

                if (kq > new TimeSpan(4, 0, 0))
                    return new TimeSpan(4, 0, 0);
                else if (kq < new TimeSpan())
                    return new TimeSpan();
                return kq;
            }
            catch (Exception)
            {
                return new TimeSpan();
            }
        }
    }
}
