﻿using QuanLy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuanLy.ModelViews
{
    public class ViewModel
    {
        public List<Staff> Staffs { get; set; }
        public List<Shift> Shifts { get; set; }
        public List<StaffContract> StaffContracts { get; set; }
        public List<Department> Departments { get; set; }
        public List<Position> Positions { get; set; }
        public List<Company> Companies { get; set; }
        public List<DownTime> DownTimes { get; set; }
        public List<StaffFile> StaffFiles { get; set; }
        public List<ComboboxString> TinhTrangNhanVien { get; set; }
        public List<ComboboxString> ThoiGianNghiPhep { get; set; }
        public List<ComboboxString> TinhTrangHoSo { get; set; }
        public List<ComboboxString> LoaiHD { get; set; }
        public List<Mission> Missions { get; set; }
        public List<City> Cities { get; set; }
        public Staff Staff { get; set; }
        public List<Candidate> Candidates { get; set; }
        public List<Degree> Degrees { get; set; }
        public List<CandidateType> CandidateTypes { get; set; }
        public List<CandidateStatus> CandidateStatuses { get; set; }
        public List<CandidateStep> CandidateSteps { get; set; }
        public List<CandidateQuestion> CandidateQuestions { get; set; }
        public List<CandidateProcess> CandidateProcesses { get; set; }
        public List<Question> Questions { get; set; }
        public List<QuestionForPosition> QuestionForPositions { get; set; }
        public List<QuestionType> QuestionTypes { get; set; }
        public List<Holiday> Holidays { get; set; }
        public List<Schedule> Schedules { get; set; }
        public List<Team> Teams { get; set; }
        public List<WorkType> WorkTypes { get; set; }
        public List<TodoList> TodoLists { get; set; }
        public List<OffsetDay> OffsetDays { get; set; }
        public List<CoinTrade> CoinTrades { get; set; }
        public ViewModel()
        {
            //LoadStaff();
            //LoadShifts();
            //LoadCompanies();
            //LoadDepartment();
            //LoadDownTimes();
            //LoadStaffContracts();
            //LoadStaffFiles();
            //LoadPositions();
            //LoadMissions();
            //LoadCity();
            //LoadCandidates();
            //LoadQuestionForPositions();
            //LoadCandidateQuestions();
            //LoadHoliday();
            //LoadSchedule();
            //LoadTodoList();
            //LoadOffsetDay();
            //TinhTrangNhanVien = Constant.TinhTrangNhanVien();
            //ThoiGianNghiPhep = Constant.ThoiGianNghiPhep();
            //TinhTrangHoSo = Constant.TinhTrangHoSo();
            //LoaiHD = Constant.LoaiHD();
            LoadCoinTrade();
        }
        public void LoadCoinTrade()
        {
            using (AppDbContext context = new AppDbContext())
            {
                CoinTrades = context.CoinTrades.OrderBy(x=> Math.Abs(x.TP1)).ThenBy(x => x.CoinName).ToList();
            }
        }
        public void LoadCoinTrade(DateTime date)
        {
            using (AppDbContext context = new AppDbContext())
            {
                CoinTrades = context.CoinTrades.Where(x=>x.Date.Date.Equals(date.Date)) .OrderBy(x => Math.Abs(x.TP1)).ThenBy(x => x.CoinName).ToList();
            }
        }

        public void LoadOffsetDay()
        {
            using (AppDbContext context = new AppDbContext())
            {
                OffsetDays = context.OffsetDays.OrderBy(x => x.Id).ToList();
            }
        }
        public void LoadTodoList()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Teams = context.Teams.OrderBy(x => x.Name).ToList();
                WorkTypes = context.WorkTypes.OrderBy(x => x.Name).ToList();
                TodoLists = context.TodoLists.OrderByDescending(x => x.Start).ToList();
            }
        }
        public void LoadStaff()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Staffs = context.Staffs.OrderBy(x => x.Code).ToList();
            }
        }

        public void LoadHoliday()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Holidays = context.Holidays.OrderBy(x=>x.Date).ToList();
            }
        }
        public void LoadSchedule()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Schedules = context.Schedules.ToList();
            }
        }
        public void LoadShifts()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Shifts = context.Shifts.ToList();
            }
        }
        public void LoadStaffContracts()
        {
            using (AppDbContext context = new AppDbContext())
            {
                StaffContracts = context.StaffContracts.ToList();
            }
        }
        public void LoadDepartment()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Departments = context.Departments.ToList();
            }
        }
        public void LoadPositions()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Positions = (from a in context.Positions
                             join b in context.Departments
                             on a.DepartmentId equals b.Id
                             select new Position
                             {
                                 Id = a.Id,
                                 Name = b.Name +" | "+a.Name,
                                 Department = b,
                                 DepartmentId = b.Id
                             }).OrderBy(x=>x.Name).ToList();
            }
        }
        public void LoadCompanies()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Companies = context.Companies.ToList();
            }
        }
        public void LoadDownTimes()
        {
            using (AppDbContext context = new AppDbContext())
            {
                DownTimes = context.DownTimes.ToList();
            }
        }
        public void LoadMissions()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Missions = context.Missions.ToList();
            }
        }
        public void LoadCity()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Cities = context.Cities.ToList();
            }
        }
        public void LoadStaffFiles()
        {
            using (AppDbContext context = new AppDbContext())
            {
                StaffFiles = context.StaffFiles.ToList();
            }
        }
        public void LoadTinhTrangNhanVien()
        {
            TinhTrangNhanVien = Constant.TinhTrangNhanVien();
        }
        public void LoadThoiGianNghiPhep()
        {
            ThoiGianNghiPhep = Constant.ThoiGianNghiPhep();
        }
        public void LoadTinhTrangHoSo()
        {
            TinhTrangHoSo = Constant.TinhTrangHoSo();
        }
        public void LoadLoaiHD()
        {
            LoaiHD = Constant.LoaiHD();
        }

        public void LoadQuestionForPositions()
        {
            using (AppDbContext context = new AppDbContext())
            {
                QuestionForPositions = context.QuestionForPositions.OrderBy(x=>x.PositionId).ToList();
            }
        }
        public void LoadCandidateQuestions()
        {
            using (AppDbContext context = new AppDbContext())
            {
                CandidateQuestions = context.CandidateQuestions.OrderBy(x=>x.CandidateId).ToList();
            }
        }
        public void LoadCandidates()
        {
            using (AppDbContext context = new AppDbContext())
            {
                Candidates = context.Candidates.ToList();
                CandidateProcesses = context.CandidateProcesses.ToList();
                CandidateStatuses = context.CandidateStatuses.ToList();
                CandidateSteps = context.CandidateSteps.ToList();
                CandidateTypes = context.CandidateTypes.ToList();
                Questions = context.Questions.ToList();
                QuestionTypes = context.QuestionTypes.ToList();
                Degrees = context.Degrees.ToList();
            }
        }
        
    }
}
