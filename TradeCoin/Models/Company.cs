﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Company
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string TaxNo { get; set; }
        [Required]
        public string Representative { get; set; } //người đại diện
        [Required]
        public string Position { get; set; } 
        public string Phone { get; set; }
        public string AuthorizationLetter { get; set; }//giấy ủy quyền
        [JsonIgnore]
        public virtual ICollection<StaffContract> StaffContracts { get; set; }
        [JsonIgnore]
        public virtual ICollection<Manager> Managers { get; set; }
    }
}
