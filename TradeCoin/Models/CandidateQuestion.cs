﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class CandidateQuestion//cau hoi riêng cho từng ứng viên
    {
        [Key]
        public int Id { get; set; }
        public int Scores { get; set; }
        [Required, Id]
        public int CandidateId { get; set; }
        [JsonIgnore]
        public virtual Candidate Candidate { get; set; }
        [Required, Id]
        public int QuestionId { get; set; }
        [JsonIgnore]
        public virtual Question Question { get; set; }
    }
}
