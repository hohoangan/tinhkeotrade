﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class StaffFile
    {
        [Key]
        public int Id { get; set; }
        public bool Done { get; set; }//khi day du tat ca ho so
        [Required, Id]
        public int StaffId { get; set; }
        [JsonIgnore]
        public Staff Staff { get; set; }
        [Required]
        public string CMND { get; set; } //Photo | Chính | Chứng thực
        [Required]
        public string BangCap { get; set; }
        [Required]
        public string SoHK { get; set; }
        [Required]
        public string SoYeuLyLich { get; set; }
        [Required]
        public string GiayKhamSucKhoe { get; set; } 
    }
}
