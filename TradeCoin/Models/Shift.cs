﻿using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Shift
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public TimeSpan Start { get; set; }
        [Required]
        public TimeSpan End { get; set; }
        public TimeSpan Sat { get; set; }
        [JsonIgnore]
        public virtual ICollection<Staff> Staffs { get; set; }

    }
}
