﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class CandidateProcess//quá trình giải quyết hồ sơ của nhân viên tới bước nào
    {
        [Key]
        public int Id { get; set; }
        public string Des { get; set; }
        public DateTime Date { get; set; }
        [Required, Id]
        public int CandidateId { get; set; }
        [JsonIgnore]
        public virtual Candidate Candidate { get; set; }
        [Required, Id]
        public int CandidateStepId { get; set; }
        [JsonIgnore]
        public virtual CandidateStep CandidateStep { get; set; }
        [Required, Id]
        public int CandidateStatusId { get; set; }
        [JsonIgnore]
        public virtual CandidateStatus CandidateStatus { get; set; }
    }
}
