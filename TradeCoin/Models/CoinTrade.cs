﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;
namespace QuanLy.Models
{
    public class CoinTrade
    {
        [Key]
        public int Id { get; set; }
        public string CoinName { get; set; }
        public double PositionSize { get; set; }
        public int Leverage { get; set; }
        public double MarginCost { get; set; }
        public double Entry { get; set; }
        public double Stoploss { get; set; }
        public double TakeProfit { get; set; }
        public double TP1 { get; set; }
        public double TP2 { get; set; }
        public DateTime Date { get; set; }
        public string Descriptions { get; set; }
    }
}
