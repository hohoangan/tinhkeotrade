﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Mission
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Reason { get; set; }
        public string Location { get; set; }
        public string ApprovedBy { get; set; }
        public string Des { get; set; }
        [Required]
        public DateTime FromDate { get; set; }
        [Required]
        public string Time { get; set; } //Morning | Afternoon | AllDay
        // neu from=to 1 ngay co the 1 or 0.5
        // neu from!=to | allday
        [Required]
        public TimeSpan TotalHours { get; set; }
        [Required, Id]
        public int StaffId { get; set; }
        [JsonIgnore]
        public Staff Staff { get; set; }
    }
}
