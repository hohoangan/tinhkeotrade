﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Manager
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public int StaffId { get; set; }
        [JsonIgnore]
        public Staff Staff { get; set; }
        [Required]
        public int DepartmentId { get; set; }
        [JsonIgnore]
        public Department Department { get; set; }
        [Required]
        public int CompanyId { get; set; }
        [JsonIgnore]
        public Company Company { get; set; }
    }
}
