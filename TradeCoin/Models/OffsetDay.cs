﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuanLy.Models
{
    public class OffsetDay
    {
        [Key]
        public int Id { get; set; }
        public int StaffId { get; set; }
        public DayOfWeek Day { get; set; }
    }
}
