﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class CandidateStatus//trạng thái của 1 step
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required, Id]
        public int CandidateStepId { get; set; }
        [JsonIgnore]
        public virtual CandidateStep CandidateStep { get; set; }
    }
}
