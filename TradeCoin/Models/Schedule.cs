﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuanLy.Models
{
    public class Schedule
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public string Des { get; set; }
    }
}
