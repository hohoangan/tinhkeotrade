﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class Candidate
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string FullName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Phone { get; set; }
        public string EmailPrivate { get; set; }
        public bool Sex { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public string Schoole { get; set; }
        [Required, Id]
        public int DegreeId { get; set; }
        [JsonIgnore]
        public virtual Degree Degree { get; set; }
        public int YearOfWork { get; set; }
        public string Company1 { get; set; }
        public string Company2 { get; set; }
        public string Des { get; set; }
        [Required, Id]
        public int PositionId { get; set; }
        [JsonIgnore]
        public Position Position { get; set; }
        [Required, Id]
        public int CandidateTypeId { get; set; }
        [JsonIgnore]
        public virtual CandidateType CandidateType { get; set; }//hình thức ứng tuyển
    }
}
