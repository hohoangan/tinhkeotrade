﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;

namespace QuanLy.Models
{
    public class TodoList
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int WorkTypeId { get; set; }
        [JsonIgnore]
        public virtual WorkType WorkType{ get; set; }
        public int TeamId { get; set; }
        [JsonIgnore]
        public virtual Team Team { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public string Des { get; set; }
    }
}
