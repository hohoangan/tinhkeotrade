﻿using QuanLy.Extensions;
using QuanLy.Models;
using QuanLy.UserControlUC;
using Syncfusion.UI.Xaml.Grid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace QuanLy
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        BackgroundWorker _backgroundWorker = new BackgroundWorker();
        public MainWindow()
        {
            #region LOGO
            this.Icon = Helps.GetImage("logo.ico");
            #endregion
            InitializeComponent();
            DateTime dt = DateTime.Now;
            DateTime dtEnd = new DateTime(2021, 12, 31);
            //if (dt > dtEnd)
            //{
            //    MessageBox.Show("Call An: 0343481281");
            //}

            if (Constant.Permission == 1)
            {
                //wrapUser.Visibility = Visibility.Collapsed;
                RadioButton_Click(null, null);
                // Set up the Background Worker Events 
                _backgroundWorker.DoWork += _backgroundWorker_DoWork;
                _backgroundWorker.RunWorkerCompleted += _backgroundWorker_RunWorkerCompleted;
                // Run the Background Worker
                _backgroundWorker.RunWorkerAsync();

                //ToolTipService.SetShowDuration(btnDownTime, 30000);
                //ToolTipService.SetShowDuration(btnMISSION, 30000);
                //ToolTipService.SetShowDuration(btnHOLIDAY, 30000);
            }
            else
            {
                wrapAdmin.Visibility = Visibility.Collapsed;
                MenuItem_Chamcong(null, null);
            }
        }

        private void _backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Cancelled)
                {
                    MessageBox.Show("Cancelled");
                }
                else if (e.Error != null)
                {
                    MessageBox.Show("Exception Thrown");
                }
                else
                    _backgroundWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void _backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                using (AppDbContext context = new AppDbContext())
                {
                    var schedule = context.Schedules.AsEnumerable().Where(x => x.Date.AddTicks(x.Time.TimeOfDay.Ticks) >= DateTime.Now)
                                                            .OrderBy(x => x.Date.AddTicks(x.Time.TimeOfDay.Ticks))
                                                            .FirstOrDefault();

                    if (schedule != null)
                    {
                        DateTime time = DateTime.Now;
                        DateTime scheduleT = schedule.Date.AddTicks(schedule.Time.TimeOfDay.Ticks);
                        int dif = (scheduleT - time).Minutes;
                        if (dif == 30 || dif == 10 || dif == 5 || dif == 0)
                        {
                            Application.Current.Dispatcher.BeginInvoke(
                            DispatcherPriority.Background,
                            new Action(() =>
                            {
                                string mess = string.Format("[{0}] {1}", scheduleT.ToString("hh':'mm"), schedule.Des);
                                PopUp popUp = new PopUp(mess, dif.ToString());
                                popUp.ShowDialog();
                            }
                            ));
                            Thread.Sleep(1000 * 50);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void ChangePalette(Uri uri, int position)
        {
            System.Windows.Application.Current.Resources.MergedDictionaries.RemoveAt(position);
            System.Windows.Application.Current.Resources.MergedDictionaries.Insert(position, new ResourceDictionary() { Source = uri });
        }

        private void MenuItem_Open(object sender, RoutedEventArgs e)
        {
            OpenUserController(new StaffUC());
        }

        private void MenuItem_SaveAs(object sender, RoutedEventArgs e)
        {
            OpenUserController(new TimeKeepingUC());
        }

        private void MenuItem_Close(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        void OpenUserController(UserControl uc)
        {
            userControl.Children.Clear();
            userControl.Children.Add(uc);
        }

        private void MenuItem_CreateContract(object sender, RoutedEventArgs e)
        {
            OpenUserController(new StaffContractUC());
        }

        private void MenuItem_LeaveRequest(object sender, RoutedEventArgs e)
        {
            OpenUserController(new DownTimeUC());
        }
        private void MenuItem_Files(object sender, RoutedEventArgs e)
        {
            OpenUserController(new StaffFileUC());
        }

        private void MenuItem_ExportContract(object sender, RoutedEventArgs e)
        {
            OpenUserController(new ContractUC());
        }

        private void btnChangeColor_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                string color = btn.Content.ToString();
                string s1 = string.Format("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Primary/MaterialDesignColor.{0}.xaml", color);
                string s2 = string.Format("pack://application:,,,/MaterialDesignColors;component/Themes/Recommended/Accent/MaterialDesignColor.{0}.xaml", color);
                Uri uriPrimary = new Uri($"" + s1);
                Uri uriAccent = new Uri($"" + s2);
                ChangePalette(uriPrimary, 2);
                ChangePalette(uriAccent, 3);
                this.Resources["AccentColor"] = (SolidColorBrush)new BrushConverter().ConvertFromString(color);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.InnerException?.Message ?? ex.Message);
            }
        }

        private void MenuItem_Mission(object sender, RoutedEventArgs e)
        {
            OpenUserController(new MissionUC());
        }

        private void MenuItem_UNGVIEN(object sender, RoutedEventArgs e)
        {
            OpenUserController(new CandidateUC());
        }

        private void MenuItem_question(object sender, RoutedEventArgs e)
        {
            OpenUserController(new QuestionUC());
        }

        private void MenuItem_question4position(object sender, RoutedEventArgs e)
        {
            OpenUserController(new QuestionForPositionUC());
        }

        private void MenuItem_question4candidate(object sender, RoutedEventArgs e)
        {
            OpenUserController(new CandidateQuestionUC());
        }

        public void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                //if (Constant.Permission == 1)
                //    Constant.windowHeight = this.ActualHeight - 250;
                //else
                //    Constant.windowHeight = this.ActualHeight - 150;

                Constant.windowHeight = this.ActualHeight - 150;
                //chinh size grid
                var grid = UIHelper.FindChildbyType<SfDataGrid>(this, "SfDataGrid");
                if (grid != null)
                    (grid as SfDataGrid).Height = Constant.windowHeight;

                var scroll = UIHelper.FindChild<ScrollViewer>(this, "scrollEvent");
                if (scroll != null)
                    (scroll as ScrollViewer).Height = Constant.windowHeight;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MenuItem_reviewCandidate(object sender, RoutedEventArgs e)
        {
            OpenUserController(new ReviewCandidateUC());
        }

        private void MenuItem_Holiday(object sender, RoutedEventArgs e)
        {
            OpenUserController(new HolidayUC());
        }

        private void MenuItem_Dashboard(object sender, RoutedEventArgs e)
        {
            OpenUserController(new Dashboard());
        }

        private void MenuItem_CandidateProcess(object sender, RoutedEventArgs e)
        {
            OpenUserController(new ReviewCandidateListUC());
        }

        private void MenuItem_Staff(object sender, RoutedEventArgs e)
        {
            OpenUserController(new NhanVienUC());
        }

        private void MenuItem_Chamcong(object sender, RoutedEventArgs e)
        {
            OpenUserController(new PunchingMachine());
        }
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                if (ChamCongConstant.bIsConnected)
                    ChamCongConstant.axCZKEM1.Disconnect();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MenuItem_todolist(object sender, RoutedEventArgs e)
        {
            OpenUserController(new TodoListUC());
        }

        private void MenuItem_todolist_rpt(object sender, RoutedEventArgs e)
        {
            OpenUserController(new TodoReportUC());
        }

        private void MenuItem_NghiPhep(object sender, RoutedEventArgs e)
        {
            OpenUserController(new MauDonUC());
        }

        private void MenuItem_Offsetday(object sender, RoutedEventArgs e)
        {
            OpenUserController(new OffsetDayUC());
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            OpenUserController(new TradeCoinUC());
        }
    }
}
