﻿using QuanLy.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using System.IO;

namespace QuanLy
{
    public class AppDbContext : DbContext
    {
        //public AppDbContext(DbContextOptions opt) : base(opt)
        //{
        //    Database.EnsureCreated();
        //}
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Create Unique Indexes
            modelBuilder.Entity<QuestionForPosition>()
            .HasIndex(p => new { p.QuestionId, p.PositionId })
            .IsUnique(true)
            .HasDatabaseName("IX_Question_Position");

            modelBuilder.Entity<CandidateProcess>()
           .HasIndex(p => new { p.CandidateId, p.CandidateStepId })
           .IsUnique(true)
           .HasDatabaseName("IX_Candidate_Step_Status");

            modelBuilder.Entity<CandidateQuestion>()
           .HasIndex(p => new { p.CandidateId, p.QuestionId })
           .IsUnique(true)
           .HasDatabaseName("IX_Candidate_QuestionId");
            #endregion
            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Data Source=../../vinstar.db"); //tu source code
            optionsBuilder.UseSqlite("Data Source=vinstar.db"); //tu folder bin or Build
            //optionsBuilder.UseSqlite("Data Source=//vinstargroup.ddns.net/tech/AnHo/DB/vinstar.db");
            //optionsBuilder.UseSqlite("Data Source=//192.168.1.199/tech/AnHo/DB/vinstar.db");
        }
        public DbSet<Staff> Staffs { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<StaffContract> StaffContracts { get; set; }
        public DbSet<DownTime> DownTimes { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<StaffFile> StaffFiles { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionForPosition> QuestionForPositions { get; set; }
        public DbSet<Degree> Degrees { get; set; }
        public DbSet<CandidateType> CandidateTypes { get; set; }
        public DbSet<CandidateStep> CandidateSteps { get; set; }
        public DbSet<CandidateStatus> CandidateStatuses { get; set; }
        public DbSet<Candidate> Candidates { get; set; }
        public DbSet<CandidateProcess> CandidateProcesses { get; set; }
        public DbSet<CandidateQuestion> CandidateQuestions { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<WorkType> WorkTypes { get; set; }
        public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<OffsetDay> OffsetDays { get; set; }
        public DbSet<CoinTrade> CoinTrades { get; set; }
    }
}
