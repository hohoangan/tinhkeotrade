﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CompanyId",
                table: "Manager",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Manager_CompanyId",
                table: "Manager",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Companies_CompanyId",
                table: "Manager",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Companies_CompanyId",
                table: "Manager");

            migrationBuilder.DropIndex(
                name: "IX_Manager_CompanyId",
                table: "Manager");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Manager");
        }
    }
}
