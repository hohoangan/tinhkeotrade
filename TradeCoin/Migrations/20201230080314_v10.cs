﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v10 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "CandidateProcesses",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Des",
                table: "CandidateProcesses",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "CandidateProcesses");

            migrationBuilder.DropColumn(
                name: "Des",
                table: "CandidateProcesses");
        }
    }
}
