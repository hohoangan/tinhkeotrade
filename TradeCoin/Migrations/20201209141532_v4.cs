﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Companies_CompanyId",
                table: "Manager");

            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Departments_DepartmentId",
                table: "Manager");

            migrationBuilder.DropForeignKey(
                name: "FK_Manager_Staffs_StaffId",
                table: "Manager");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Manager",
                table: "Manager");

            migrationBuilder.RenameTable(
                name: "Manager",
                newName: "Managers");

            migrationBuilder.RenameIndex(
                name: "IX_Manager_StaffId",
                table: "Managers",
                newName: "IX_Managers_StaffId");

            migrationBuilder.RenameIndex(
                name: "IX_Manager_DepartmentId",
                table: "Managers",
                newName: "IX_Managers_DepartmentId");

            migrationBuilder.RenameIndex(
                name: "IX_Manager_CompanyId",
                table: "Managers",
                newName: "IX_Managers_CompanyId");

            migrationBuilder.AddColumn<string>(
                name: "EmailPrivate",
                table: "Staffs",
                type: "TEXT",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Managers",
                table: "Managers",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Managers_Companies_CompanyId",
                table: "Managers",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Managers_Departments_DepartmentId",
                table: "Managers",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Managers_Staffs_StaffId",
                table: "Managers",
                column: "StaffId",
                principalTable: "Staffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Managers_Companies_CompanyId",
                table: "Managers");

            migrationBuilder.DropForeignKey(
                name: "FK_Managers_Departments_DepartmentId",
                table: "Managers");

            migrationBuilder.DropForeignKey(
                name: "FK_Managers_Staffs_StaffId",
                table: "Managers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Managers",
                table: "Managers");

            migrationBuilder.DropColumn(
                name: "EmailPrivate",
                table: "Staffs");

            migrationBuilder.RenameTable(
                name: "Managers",
                newName: "Manager");

            migrationBuilder.RenameIndex(
                name: "IX_Managers_StaffId",
                table: "Manager",
                newName: "IX_Manager_StaffId");

            migrationBuilder.RenameIndex(
                name: "IX_Managers_DepartmentId",
                table: "Manager",
                newName: "IX_Manager_DepartmentId");

            migrationBuilder.RenameIndex(
                name: "IX_Managers_CompanyId",
                table: "Manager",
                newName: "IX_Manager_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Manager",
                table: "Manager",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Companies_CompanyId",
                table: "Manager",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Departments_DepartmentId",
                table: "Manager",
                column: "DepartmentId",
                principalTable: "Departments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Manager_Staffs_StaffId",
                table: "Manager",
                column: "StaffId",
                principalTable: "Staffs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
