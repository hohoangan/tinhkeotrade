﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Candidates",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FullName = table.Column<string>(type: "TEXT", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Sex = table.Column<bool>(type: "INTEGER", nullable: false),
                    Country = table.Column<string>(type: "TEXT", nullable: true),
                    Address = table.Column<string>(type: "TEXT", nullable: true),
                    Schoole = table.Column<string>(type: "TEXT", nullable: true),
                    DegreeId = table.Column<int>(type: "INTEGER", nullable: false),
                    YearOfWork = table.Column<int>(type: "INTEGER", nullable: false),
                    Company1 = table.Column<string>(type: "TEXT", nullable: true),
                    Company2 = table.Column<string>(type: "TEXT", nullable: true),
                    Des = table.Column<string>(type: "TEXT", nullable: true),
                    PositionId = table.Column<int>(type: "INTEGER", nullable: false),
                    CandidateTypeId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Candidates", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Candidates_CandidateTypes_CandidateTypeId",
                        column: x => x.CandidateTypeId,
                        principalTable: "CandidateTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Candidates_Degrees_DegreeId",
                        column: x => x.DegreeId,
                        principalTable: "Degrees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Candidates_Positions_PositionId",
                        column: x => x.PositionId,
                        principalTable: "Positions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CandidateProcesses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CandidateId = table.Column<int>(type: "INTEGER", nullable: false),
                    CandidateStepId = table.Column<int>(type: "INTEGER", nullable: false),
                    CandidateStatusId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CandidateProcesses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CandidateProcesses_Candidates_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CandidateProcesses_CandidateStatuses_CandidateStatusId",
                        column: x => x.CandidateStatusId,
                        principalTable: "CandidateStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CandidateProcesses_CandidateSteps_CandidateStepId",
                        column: x => x.CandidateStepId,
                        principalTable: "CandidateSteps",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CandidateQuestion",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Scores = table.Column<int>(type: "INTEGER", nullable: false),
                    CandidateId = table.Column<int>(type: "INTEGER", nullable: false),
                    QuestionId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CandidateQuestion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CandidateQuestion_Candidates_CandidateId",
                        column: x => x.CandidateId,
                        principalTable: "Candidates",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CandidateQuestion_Questions_QuestionId",
                        column: x => x.QuestionId,
                        principalTable: "Questions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Candidate_Step_Status",
                table: "CandidateProcesses",
                columns: new[] { "CandidateId", "CandidateStatusId", "CandidateStepId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CandidateProcesses_CandidateStatusId",
                table: "CandidateProcesses",
                column: "CandidateStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateProcesses_CandidateStepId",
                table: "CandidateProcesses",
                column: "CandidateStepId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateQuestion_CandidateId",
                table: "CandidateQuestion",
                column: "CandidateId");

            migrationBuilder.CreateIndex(
                name: "IX_CandidateQuestion_QuestionId",
                table: "CandidateQuestion",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_Candidates_CandidateTypeId",
                table: "Candidates",
                column: "CandidateTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Candidates_DegreeId",
                table: "Candidates",
                column: "DegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_Candidates_PositionId",
                table: "Candidates",
                column: "PositionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CandidateProcesses");

            migrationBuilder.DropTable(
                name: "CandidateQuestion");

            migrationBuilder.DropTable(
                name: "Candidates");
        }
    }
}
