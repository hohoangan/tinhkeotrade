﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateQuestion_Candidates_CandidateId",
                table: "CandidateQuestion");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateQuestion_Questions_QuestionId",
                table: "CandidateQuestion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateQuestion",
                table: "CandidateQuestion");

            migrationBuilder.RenameTable(
                name: "CandidateQuestion",
                newName: "CandidateQuestions");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateQuestion_QuestionId",
                table: "CandidateQuestions",
                newName: "IX_CandidateQuestions_QuestionId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateQuestion_CandidateId",
                table: "CandidateQuestions",
                newName: "IX_CandidateQuestions_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateQuestions",
                table: "CandidateQuestions",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateQuestions_Candidates_CandidateId",
                table: "CandidateQuestions",
                column: "CandidateId",
                principalTable: "Candidates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateQuestions_Questions_QuestionId",
                table: "CandidateQuestions",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CandidateQuestions_Candidates_CandidateId",
                table: "CandidateQuestions");

            migrationBuilder.DropForeignKey(
                name: "FK_CandidateQuestions_Questions_QuestionId",
                table: "CandidateQuestions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CandidateQuestions",
                table: "CandidateQuestions");

            migrationBuilder.RenameTable(
                name: "CandidateQuestions",
                newName: "CandidateQuestion");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateQuestions_QuestionId",
                table: "CandidateQuestion",
                newName: "IX_CandidateQuestion_QuestionId");

            migrationBuilder.RenameIndex(
                name: "IX_CandidateQuestions_CandidateId",
                table: "CandidateQuestion",
                newName: "IX_CandidateQuestion_CandidateId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CandidateQuestion",
                table: "CandidateQuestion",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateQuestion_Candidates_CandidateId",
                table: "CandidateQuestion",
                column: "CandidateId",
                principalTable: "Candidates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CandidateQuestion_Questions_QuestionId",
                table: "CandidateQuestion",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
