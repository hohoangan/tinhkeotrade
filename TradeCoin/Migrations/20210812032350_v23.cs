﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace QuanLy.Migrations
{
    public partial class v23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CoinTrades",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CoinName = table.Column<string>(type: "TEXT", nullable: true),
                    PositionSize = table.Column<double>(type: "REAL", nullable: false),
                    Leverage = table.Column<int>(type: "INTEGER", nullable: false),
                    MarginCost = table.Column<double>(type: "REAL", nullable: false),
                    Entry = table.Column<double>(type: "REAL", nullable: false),
                    Stoploss = table.Column<double>(type: "REAL", nullable: false),
                    TakeProfit = table.Column<double>(type: "REAL", nullable: false),
                    TP1 = table.Column<double>(type: "REAL", nullable: false),
                    TP2 = table.Column<double>(type: "REAL", nullable: false),
                    Date = table.Column<DateTime>(type: "TEXT", nullable: false),
                    Descriptions = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoinTrades", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoinTrades");
        }
    }
}
